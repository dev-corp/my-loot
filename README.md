# MyLoot

## Setup

// TODO

## Build

// TODO

## Run

### Alternatives

To run api from `/dist` run `nodemon dist/web/api/api.bundle.js`

## DB Migrations

Available connections (`ormconfig.json`):
- `sqlite`

Generate migrations: 

`npm run typeorm -- migration:generate --connection <connection> --name <migration_name>`

List migrations: 

`npm run typeorm -- migration:show -c sqlite`

Run migrations: 

`npm run typeorm -- migration:run --connection <connection> --name <migration_name>`

