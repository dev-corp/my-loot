import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { StoreUtil } from '@devcorp-libs/store-util';
import { AccountDialogComponent } from 'src/app/components';
import { AccountService } from 'src/app/services';
import { Account } from 'shared/models';
import { IState } from 'src/app/+state';
import { AppActions, FromAppState } from 'src/app/+state/app';
import { Router } from '@angular/router';
import { TransactionsPageActions } from 'src/app/transactions-page';


@Component({
  selector: 'ml-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {

  public readonly accounts$: Observable<Account[]>;

  constructor(private store: Store<IState>,
              private router: Router,
              private dialog: MatDialog,
              private accountService: AccountService) {
    this.store.dispatch(AppActions.loadAll());
    this.accounts$ = StoreUtil.select(this.store, FromAppState.selectAccounts);
  }

  public createAccount(): void {
    const dialogRef = this.dialog.open<AccountDialogComponent, any, Account>(AccountDialogComponent);

    dialogRef.afterClosed()
      .pipe(
        filter((r) => !!r),
        switchMap((account: Account) => this.accountService.createAccount(account)),
      )
      .subscribe((account) => {
        this.store.dispatch(AppActions.loadAccounts());
        this.store.dispatch(AppActions.loadPayees());
        this.router.navigate(['transactions/account', account.id]);
      });
  }

  public updateAccount(event: Event, account: Account): void {
    event.preventDefault();
    const dialogRef = this.dialog.open<AccountDialogComponent, Account, Account>(AccountDialogComponent, {
      data: account,
    });
    dialogRef.afterClosed()
      .pipe(
        filter((r) => !!r),
        switchMap((acc: Account) => this.accountService.updateAccount(acc)),
      )
      .subscribe(async () => {
        this.store.dispatch(AppActions.loadAccounts());
        this.router.onSameUrlNavigation = 'reload';
        this.store.dispatch(TransactionsPageActions.loadTransactions());
      });
  }
}
