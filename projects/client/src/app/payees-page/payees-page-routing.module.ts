import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayeesPageComponent } from './payees-page.component';


const routes: Routes = [
  { path: '', component: PayeesPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayeesPageRoutingModule {}
