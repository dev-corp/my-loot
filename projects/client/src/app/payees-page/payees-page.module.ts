import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayeesPageRoutingModule } from './payees-page-routing.module';
import { PayeesPageComponent } from './payees-page.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [PayeesPageComponent],
    imports: [
        CommonModule,
        PayeesPageRoutingModule,
        MatToolbarModule,
        MatListModule,
        MatCardModule,
    ],
})
export class PayeesPageModule {}
