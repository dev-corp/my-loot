import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IState } from 'src/app/+state';
import { StoreUtil } from '@devcorp-libs/store-util';
import { AppActions, FromAppState } from 'src/app/+state/app';
import { NgUnsubscribe } from '@devcorp-libs/ng-unsubscribe';
import { Observable } from 'rxjs';
import { Payee } from 'shared/models';
import { map } from 'rxjs/operators';


@Component({
  selector: 'ml-payees-page',
  templateUrl: './payees-page.component.html',
  styleUrls: ['./payees-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PayeesPageComponent extends NgUnsubscribe {

  public readonly payees$: Observable<Payee[]>;

  constructor(private store: Store<IState>) {
    super();
    this.payees$ = StoreUtil.select(store, FromAppState.selectPayees, this.ngUnsubscribe)
      .pipe(
        map((payees) => payees.filter((payee) => !payee.isSystem && !payee.isAccount)),
      );

    this.store.dispatch(AppActions.loadPayees());
  }

}
