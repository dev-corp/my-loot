import { AbstractControl, ValidatorFn } from '@angular/forms';


export function improperTypeValidator(type: any): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (!control.value) {
      return null;
    }
    // tslint:disable-next-line:object-literal-key-quotes
    return control.value instanceof type ? null : { 'improperType': { type: control.value.constructor.name } };
  };
}
