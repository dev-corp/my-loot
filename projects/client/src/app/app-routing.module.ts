import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'transactions', loadChildren: () => import('src/app/transactions-page').then((m) => m.TransactionsPageModule) },
  { path: 'payees', loadChildren: () => import('src/app/payees-page').then((m) => m.PayeesPageModule) },
  { path: 'categories', loadChildren: () => import('src/app/categories-page').then((m) => m.CategoriesPageModule) },
  { path: 'reports', loadChildren: () => import('src/app/reports-page').then((m) => m.ReportsPageModule) },
  { path: 'test', loadChildren: () => import('src/app/test-page').then((m) => m.TestPageModule) },
  { path: '', redirectTo: 'transactions/all', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
