import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { Account, Category, Payee, Transaction } from 'shared/models';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { StoreUtil } from '@devcorp-libs/store-util';
import { NgUnsubscribe } from '@devcorp-libs/ng-unsubscribe';
import { debounceTime, filter, map, switchMap, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ISort, SortDirection } from 'shared/search';
import { AppActions, FromAppState } from 'src/app/+state/app';
import { Store } from '@ngrx/store';
import { FromTransactionPageState, IRootAndTransactionsFeatureState } from 'src/app/transactions-page/+state';
import { improperTypeValidator } from 'src/app/validators';
import { CategoryDialogComponent, CategoryDialogData } from 'src/app/components/category-dialog';
import { AutocompleteUtilService, CategoryService } from 'src/app/services';
import { PayeeDisplayNamePipe } from 'src/app/pipes/payee-display-name';


@Component({
  selector: 'ml-transactions-table',
  templateUrl: './transactions-table.component.html',
  styleUrls: ['./transactions-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsTableComponent extends NgUnsubscribe implements OnInit {

  @Input()
  public set transactions(value: Transaction[]) {
    this._transactions = value;
    this.dataSource = new MatTableDataSource(value);
    this.selection.clear();
    this.cancelEditing();
    setTimeout(() => this.scrollToBottom());
  }

  @Output()
  public selectionChange: EventEmitter<Transaction[]> = new EventEmitter();

  @Output()
  public sortChange: EventEmitter<ISort[]> = new EventEmitter();

  @Output()
  public save: EventEmitter<Transaction> = new EventEmitter();

  @Output()
  public delete: EventEmitter<Transaction[]> = new EventEmitter();

  public dataSource: MatTableDataSource<Transaction>;
  public selection: SelectionModel<Transaction> = new SelectionModel(true, [], true);
  public columnsToDisplay: string[] = ['select', 'account', 'date', 'payee', 'category', 'memo', 'outflow', 'inflow'];

  public appearance: MatFormFieldAppearance = 'outline';

  public transactionForm: FormGroup = new FormGroup({
    account: new FormControl(null, [improperTypeValidator(Account)]),
    date: new FormControl(),
    payee: new FormControl(),
    category: new FormControl(null, [improperTypeValidator(Category)]),
    memo: new FormControl(),
    amount: new FormControl(),
    outflow: new FormControl(),
    inflow: new FormControl(),
  });

  public set formModalOpen(value: boolean) {
    if (value === false) {
      // Set to false after key
      setTimeout(() => this._formModalOpen = value, 200);
    } else {
      this._formModalOpen = value;
    }
  }

  public get formModalOpen(): boolean {
    return this._formModalOpen;
  }

  public readonly accounts$: Observable<Account[]>;
  public readonly payees$: Observable<Payee[]>;
  public readonly childrenCategories$: Observable<Category[]>;
  public readonly accountsFilteredOptions$: Observable<Account[]>;
  public readonly currentAccount$: Observable<Account>;

  public readonly childrenCategoriesFilteredOptions$: Observable<Category[]>;
  public readonly payeesFilteredOptions$: Observable<Payee[]>;

  private _transactions: Transaction[];
  private _editingTransaction: Transaction;
  private _formModalOpen: boolean = false;

  constructor(private store: Store<IRootAndTransactionsFeatureState>,
              private dialog: MatDialog,
              private elementRef: ElementRef<HTMLElement>,
              private categoryService: CategoryService,
              private autocompleteUtil: AutocompleteUtilService,
              private payeeDisplayNamePipe: PayeeDisplayNamePipe) {
    super();
    this.accounts$ = StoreUtil.select(this.store, FromAppState.selectAccounts, this.ngUnsubscribe);
    this.payees$ = StoreUtil.select(this.store, FromAppState.selectPayees, this.ngUnsubscribe)
      .pipe(
        withLatestFrom(StoreUtil.select(this.store, FromTransactionPageState.selectCurrentAccount, this.ngUnsubscribe)),
        map(([payees, currentAccount]) => currentAccount ? payees.filter((payee) => payee.id !== currentAccount.id) : payees),
        map((payees) => payees.filter((payee) => !payee.isSystem)),
      );
    this.childrenCategories$ = StoreUtil.select(this.store, FromAppState.selectChildrenCategories, this.ngUnsubscribe);
    this.currentAccount$ = StoreUtil.select(this.store, FromTransactionPageState.selectCurrentAccount, this.ngUnsubscribe);

    this.accountsFilteredOptions$ = this.autocompleteUtil.filterAutocompleteOptions<Account>(this.transactionForm.controls.account,
      this.accounts$, this.autocompleteUtil.accountAutocompleteMapper, this.ngUnsubscribe);

    this.childrenCategoriesFilteredOptions$ = this.autocompleteUtil.filterAutocompleteOptions<Category>(this.transactionForm.controls.category,
      this.childrenCategories$, this.autocompleteUtil.childrenCategoryAutocompleteMapper, this.ngUnsubscribe);

    this.payeesFilteredOptions$ = this.autocompleteUtil.filterAutocompleteOptions<Payee>(this.transactionForm.controls.payee, this.payees$,
      (value: string | Payee, options: Payee[]) => {
        if (value === null) { return options; }
        const parsedValue = value instanceof Payee ? this.payeeDisplayNamePipe.transform(value) : value;
        return options.filter((payee) => this.payeeDisplayNamePipe.transform(payee).toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase()));
      }, this.ngUnsubscribe);
  }

  ngOnInit(): void {
    // Emit selected transactions
    this.selection.changed
      .pipe(
        takeUntil(this.ngUnsubscribe),
        debounceTime(0),
      )
      .subscribe(() => { this.selectionChange.emit(this.selection.selected); });

    this.currentAccount$
      .pipe(
        takeUntil(this.ngUnsubscribe),
      )
      .subscribe((currentAccount) => {
        if (currentAccount) {
          const idx = this.columnsToDisplay.indexOf('account');
          if (idx >= 0) {
            this.columnsToDisplay.splice(idx, 1);
          }
        } else {
          const idx = this.columnsToDisplay.indexOf('account');
          if (idx === -1) {
            this.columnsToDisplay.splice(1, 0, 'account');
          }
        }
      });
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyupEvent(event: KeyboardEvent): void {
    switch (event.code) {
      case 'Enter': {
        if (this.isAnyEditing() && !this.formModalOpen) {
          this.saveTransaction();
        }
        break;
      }
      case 'Escape': {
        if (this.isAnyEditing() && !this.formModalOpen) {
          this.cancelEditing();
        }
        break;
      }
      case 'Delete': {
        if (this.selection.selected.length > 0) {
          this.delete.emit(this.selection.selected);
        }
        break;
      }
    }
  }

  public isTransactionIncomplete(transaction: Transaction): boolean {
    return !transaction.account || !transaction.date || !transaction.payee
      || !transaction.amount || (!transaction.isTransfer() && !transaction.category);
  }

  public get editingTransaction(): Transaction {
    return this._editingTransaction;
  }

  public set editingTransaction(value: Transaction) {
    // set
    this._editingTransaction = value;
    // prepare form
    if (this._editingTransaction) {
      this.addActionColumn();
      let inflow = null;
      let outflow = null;
      if (this._editingTransaction.amount >= 0) {
        inflow = this._editingTransaction.amount;
      } else if (this._editingTransaction.amount < 0) {
        outflow = this._editingTransaction.amount * -1;
      }
      this.transactionForm.reset({ ...this._editingTransaction, ...{ inflow, outflow } });
    } else {
      this.removeActionColumn();
      this.transactionForm.reset();
    }
  }

  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach((rowData) => this.selection.select(rowData));
  }

  public onSortChanges(sort: Sort): void {
    if (sort.direction) {
      let sortField = sort.active;
      if (['outflow', 'inflow'].includes(sort.active)) {
        sortField = 'amount';
      }
      const sortBy: ISort = {
        order: sort.direction.toUpperCase() as SortDirection,
        key: this.mapColumnToSortKey(sortField),
      };
      this.sortChange.emit([sortBy]);
    } else {
      this.sortChange.emit([]);
    }
  }

  public openCategoryDialog(transaction: Transaction): void {
    const newCategory = new Category({
      name: typeof this.transactionForm.value.category === 'string' ? this.transactionForm.value.category : null,
    });

    const dialogRef = this.dialog.open<CategoryDialogComponent, CategoryDialogData, Category>(CategoryDialogComponent, {
      data: { category: newCategory, type: 'child' },
    });
    dialogRef.afterClosed()
      .pipe(
        filter((category) => category instanceof Category),
        switchMap((category) => this.categoryService.createCategory(category)),
      )
      .subscribe((category) => {
        // Check if different transaction was not selected in the mean time
        if (this.editingTransaction && this.editingTransaction === transaction) {
          this.transactionForm.controls.category.patchValue(category);
          this.store.dispatch(AppActions.loadCategories());
        }
      });
  }

  public addTransaction(): void {
    const transaction = new Transaction();
    StoreUtil.select(this.store, FromTransactionPageState.selectCurrentAccount, this.ngUnsubscribe)
      .pipe(take(1))
      .subscribe((account) => {
        transaction.account = account;
        this.dataSource = new MatTableDataSource([...this.dataSource.data, transaction]);
        this.editingTransaction = transaction;
        this.focusFirstInput();
      });
  }

  public editTransaction(transaction: Transaction): void {
    if (this.isEditing(transaction)) {
      return;
    }

    if (this.isAnyEditing()) {
      this.cancelEditing();
    }

    this.selection.deselect(transaction); // prevent editing transaction to be selected
    this.editingTransaction = transaction;
  }

  public saveTransaction(): void {
    // If payee does not exist create one from string
    if (typeof this.transactionForm.value.payee === 'string') {
      if (this.transactionForm.value.payee.trim().length > 0) {
        this.transactionForm.controls.payee.patchValue(new Payee({ name: this.transactionForm.value.payee }));
      } else {
        this.transactionForm.controls.payee.patchValue(null);
      }
    }

    if (this.transactionForm.value.memo && this.transactionForm.value.memo.trim().length === 0) {
      this.transactionForm.controls.memo.patchValue(null);
    }

    if (typeof this.transactionForm.value.inflow === 'number') {
      this.transactionForm.value.amount = this.transactionForm.value.inflow;
    } else if (typeof this.transactionForm.value.outflow === 'number') {
      this.transactionForm.value.amount = this.transactionForm.value.outflow * -1;
    } else {
      this.transactionForm.value.amount = null;
    }

    const mergedTransaction = { ...this.editingTransaction, ...this.transactionForm.value };
    this.save.emit(mergedTransaction);
    this.editingTransaction = null;
  }

  public cancelEditing(): void {
    if (!this.editingTransaction) {
      return;
    }
    // remove new transaction from table
    if (!this.editingTransaction.id) {
      this.dataSource = new MatTableDataSource(this._transactions);
    }
    this.editingTransaction = null;
  }

  public isAnyEditing(): boolean {
    return !!this.editingTransaction;
  }

  public isEditing(transaction: Transaction): boolean {
    return this.editingTransaction ? this.editingTransaction.id === transaction.id : false;
  }

  public displayAccountFn(account?: Account): string | undefined {
    return account ? account.name : undefined;
  }

  public displayCategoryFn(category?: Category): string | undefined {
    return category ? category.name : undefined;
  }

  public displayPayeeFnBuilder(): (payee?: Payee) => string | undefined {
    return (payee?: Payee) => payee ? this.payeeDisplayNamePipe.transform(payee) : undefined;
  }

  public showAddCategoryButton(): boolean {
    return !this.transactionForm.value.category || typeof this.transactionForm.value.category === 'string';
  }

  private addActionColumn(): void {
    const actionsColumn = 'actions';
    if (!this.columnsToDisplay.includes(actionsColumn)) {
      this.columnsToDisplay[this.columnsToDisplay.length] = actionsColumn;
    }
  }

  private mapColumnToSortKey(key: string): string {
    switch (key) {
      case 'account':
        return 'account.name';
      case 'payee':
        return 'payee.name';
      case 'category':
        return 'category.name';
      default:
        return key;
    }
  }

  private removeActionColumn(): void {
    const actionsColumn = 'actions';
    const idx = this.columnsToDisplay.indexOf(actionsColumn);
    if (idx > -1) {
      this.columnsToDisplay.splice(idx, 1);
    }
  }

  // Focuses on account input field
  private focusFirstInput(): void {
    try {
      setTimeout(() => {
        const formInputs = this.elementRef.nativeElement.querySelectorAll('.ml-transactions-table__row--new-transaction input');
        const firstInput = formInputs.item(1) as HTMLInputElement;
        firstInput.focus();
      });
    } catch (err) {
      console.log(err);
    }
  }

  private scrollToBottom(): void {
    this.elementRef.nativeElement.scrollTop = this.elementRef.nativeElement.scrollHeight;
  }
}
