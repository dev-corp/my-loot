import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldAppearance, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { transactionPageReducer, TransactionsPageEffects } from './+state';
import { TransactionsPageComponent } from './transactions-page.component';
import { TransactionsPageRoutingModule } from './transactions-page-routing.module';
import { TransactionsTableComponent } from './components/transactions-table';
import { CategoryDialogModule, GenericDialogModule } from 'src/app/components';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { PayeeDisplayNamePipeModule, PayeeDisplayNamePipe } from 'src/app/pipes/payee-display-name';
import { StringPipeModule } from 'src/app/pipes/string-pipe';


@NgModule({
  declarations: [
    TransactionsPageComponent,
    TransactionsTableComponent,
  ],
  imports: [
    CommonModule,
    TransactionsPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature('transactionsPage', transactionPageReducer),
    EffectsModule.forFeature([TransactionsPageEffects]),
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatSortModule,
    MatMenuModule,
    MatTooltipModule,
    MatDatepickerModule,
    CategoryDialogModule,
    GenericDialogModule,
    MatChipsModule,
    MatListModule,
    PayeeDisplayNamePipeModule,
    StringPipeModule,
  ],
  providers: [
    // Needed for pagination input to be legacy
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'legacy' as MatFormFieldAppearance } },
    PayeeDisplayNamePipe,
    DatePipe,
  ],
})
export class TransactionsPageModule {}
