import { ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable, of } from 'rxjs';
import { NgUnsubscribe } from '@devcorp-libs/ng-unsubscribe';
import { StoreUtil } from '@devcorp-libs/store-util';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import {
  AutocompleteUtilService,
  ITransactionSearchFilter,
  QueryService,
  TransactionSearchService,
  TransactionService,
} from 'src/app/services';
import { Account, Transaction } from 'shared/models';
import { FilterOperator, IFilter, IQuery, ISort, QueryUtility } from 'shared/search';
import { AppActions, FromAppState } from 'src/app/+state/app';
import { FromTransactionPageState, IRootAndTransactionsFeatureState, TransactionsPageActions } from './+state';
import { MatDialog } from '@angular/material/dialog';
import { GenericDialogButtons, GenericDialogComponent, IGenericDialogOptions } from 'src/app/components/generic-dialog';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { DatePipe } from '@angular/common';

/*
  NOTE: To Do List
  - persistence query state per account view
  - search / query / filter
  - delete transaction (press delete key) + confirmation or undo delete
  - bug: category field - hide add button after creating new category
 */

@Component({
  selector: 'ml-transactions-page',
  templateUrl: './transactions-page.component.html',
  styleUrls: ['./transactions-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsPageComponent extends NgUnsubscribe implements OnInit {

  public readonly accounts$: Observable<Account[]>;
  public readonly accountsToMove$: Observable<Account[]>;
  public readonly transactions$: Observable<Transaction[]>;
  public readonly total$: Observable<number>;
  public readonly query$: Observable<IQuery>;
  public readonly searchFilters$: Observable<IFilter[]>;
  public readonly dateFrom$: Observable<Date>;
  public readonly dateTo$: Observable<Date>;

  public selectedTransaction: Transaction[] = [];

  public searchControl: FormControl = new FormControl();
  public readonly searchFilteredOptions$: Observable<ITransactionSearchFilter[]>;
  public readonly searchableItems$: Observable<ITransactionSearchFilter[]>;

  @ViewChild('searchInput')
  private searchInput: ElementRef<HTMLInputElement>;

  private readonly currentAccount$: Observable<Account>;
  private readonly urlQuery$: Observable<IQuery>;

  constructor(private store: Store<IRootAndTransactionsFeatureState>,
              private route: ActivatedRoute,
              private router: Router,
              private queryService: QueryService,
              private transactionService: TransactionService,
              private autocompleteUtil: AutocompleteUtilService,
              private dialog: MatDialog,
              private transactionSearchService: TransactionSearchService,
              private datePipe: DatePipe) {
    super();
    this.accounts$ = StoreUtil.select(this.store, FromAppState.selectAccounts, this.ngUnsubscribe);
    this.transactions$ = StoreUtil.select(this.store, FromTransactionPageState.selectTransactions, this.ngUnsubscribe);
    this.total$ = StoreUtil.select(this.store, FromTransactionPageState.selectTotal, this.ngUnsubscribe);
    this.query$ = StoreUtil.select(this.store, FromTransactionPageState.selectQuery, this.ngUnsubscribe);
    this.currentAccount$ = StoreUtil.select(this.store, FromTransactionPageState.selectCurrentAccount, this.ngUnsubscribe);
    this.dateFrom$ = StoreUtil.select(this.store, FromTransactionPageState.selectQueryDateFrom, this.ngUnsubscribe);
    this.dateTo$ = StoreUtil.select(this.store, FromTransactionPageState.selectQueryDateTo, this.ngUnsubscribe);

    this.accountsToMove$ = combineLatest([this.accounts$, this.currentAccount$])
      .pipe(
        map(([accounts, currentAccount]) => currentAccount
          ? accounts.filter((account) => account.id !== currentAccount.id) : accounts),
      );

    this.urlQuery$ = combineLatest([this.route.queryParams, this.currentAccount$])
      .pipe(
        map(([queryParams, currentAccount]) => {
          const query = QueryUtility.queryParamsToQuery(queryParams);
          // When on the account page ignore any query params related
          // to account and set current account in filter
          if (currentAccount) {
            query.filterBy = query.filterBy.filter((f) => !f.key.includes('account'));
            // @ts-ignore
            query.filterBy.push({ key: 'account.id', value: currentAccount.id, operator: FilterOperator.Equal });
          }

          // Default sort by date
          if (!query.sortBy || query.sortBy.length === 0) {
            query.sortBy = [{ key: 'date', order: 'ASC' }];
          }

          return query;
        }),
      );

    this.searchFilters$ = combineLatest([this.urlQuery$.pipe(), this.currentAccount$])
      .pipe(
        map(([query, currentAccount]) =>
          (currentAccount
            // Filter out any account filters if current account is selected
            ? query.filterBy.filter((filter) => !filter.key.includes('account'))
            : query.filterBy)
            .filter((filter) => !(filter.key === 'date' && filter.operator.match(/[<>]=/)))),
      );

    this.searchableItems$ = combineLatest([this.transactionSearchService.transactionSearchableItems(this.ngUnsubscribe),
      StoreUtil.select(this.store, FromTransactionPageState.selectCurrentAccount, this.ngUnsubscribe)])
      .pipe(
        // Filter if currently on specific account transaction page
        map(([searchItems, currentAccount]) => currentAccount
          ? searchItems.filter((item) => !(item.object instanceof Account))
          : searchItems),
      );

    this.searchFilteredOptions$ = this.autocompleteUtil.filterAutocompleteOptions<ITransactionSearchFilter>(this.searchControl,
      this.searchableItems$, this.transactionSearchService.filterOptionMapperFactory(), this.ngUnsubscribe);
  }

  ngOnInit(): void {
    this.urlQuery$
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((query) => {
        // TODO: Retrieve date query params
        this.resetSearchForm();
        this.store.dispatch(TransactionsPageActions.setQuery({ query }));
      });
  }

  public onSave(transaction: Transaction): void {
    let apiCall;
    if (transaction.id) {
      // edit existing
      apiCall = this.transactionService.updateTransaction(transaction);
    } else {
      // add new
      apiCall = this.transactionService.createTransaction(transaction);
    }
    apiCall.subscribe(() => {
      this.store.dispatch(TransactionsPageActions.loadTransactions());
      this.store.dispatch(AppActions.loadAccounts());
      // TODO: If new payee -> reload payees
    });
  }

  public onDelete(transactions: Transaction[]): void {
    if (!transactions || !transactions.length) {
      return;
    }

    const confirmDialogRef = this.dialog.open<GenericDialogComponent, IGenericDialogOptions, string>(GenericDialogComponent, {
      data: { buttons: GenericDialogButtons.YesCancel, message: `Are you sure to delete ${transactions.length} transactions?` },
    });

    confirmDialogRef.afterClosed()
      .pipe(
        switchMap((result) => {
          const doDelete = result === 'yes';
          if (doDelete) {
            return transactions.length === 1
              ? this.transactionService.deleteTransaction(transactions[0])
              : this.transactionService.deleteMultipleTransactions(transactions);
          }
          return of('cancel');
        }),
      )
      .subscribe((result) => {
        if (result !== 'cancel') {
          this.store.dispatch(TransactionsPageActions.loadTransactions());
          this.store.dispatch(AppActions.loadAccounts());
        }
      });
  }

  public moveToAccount(account: Account): void {
    const updated = this.selectedTransaction.map((transaction) => {
      return new Transaction({ ...transaction, ...{ account } });
    });
    this.transactionService.updateMultipleTransactions(updated)
      .subscribe(() => {
        this.store.dispatch(TransactionsPageActions.loadTransactions());
        this.store.dispatch(AppActions.loadAccounts());
      });
  }

  public onSortChanges(sortBy: ISort[]): void {
    let queryParams = { ...this.route.snapshot.queryParams };
    const query = this.queryService.queryParamsToQuery(queryParams);
    query.sortBy = sortBy;
    queryParams = this.queryService.queryToUrlQueryParams(query);

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      // queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
  }

  public onPageChanges(pageEvent: PageEvent): void {
    const queryParams = { ...this.route.snapshot.queryParams };
    queryParams._limit = pageEvent.pageSize;
    queryParams._page = pageEvent.pageIndex;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
  }

  public onSearchOptionSelected(event: MatAutocompleteSelectedEvent): void {
    const filter = event.option.value as ITransactionSearchFilter;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: this.queryService.queryToUrlQueryParams({ filterBy: [filter] }),
      queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
    this.resetSearchForm();
  };

  public removeFilter(filter: IFilter): void {
    const queryParams = { ...this.route.snapshot.queryParams };
    delete queryParams[filter.key];
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      skipLocationChange: false,
    });
  }

  public removeSearchFilters(): void {
    const queryParams = { ...this.route.snapshot.queryParams };
    // removing all search filters excluding _* and date
    Object.keys(queryParams)
      .filter((key) => !key.startsWith('_') && key !== 'date')
      .forEach((key) => {
        delete queryParams[key];
      });
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      skipLocationChange: false,
    });
  }

  public dateThisMonthSelected(): void {
    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth();
    const daysInMonth = new Date(year, month + 1, 0).getDate();

    const dateFrom = this.dateToSimpleIsoString(new Date(year, month, 1));
    const dateTo = this.dateToSimpleIsoString(new Date(year, month, daysInMonth));

    let queryParams = { ...this.route.snapshot.queryParams };
    const query = this.queryService.queryParamsToQuery(queryParams);

    query.filterBy = query.filterBy.filter((filter) => filter.key !== 'date');
    query.filterBy.push({ key: 'date', operator: FilterOperator.GreaterThanOrEqual, value: dateFrom });
    query.filterBy.push({ key: 'date', operator: FilterOperator.LessThanOrEqual, value: dateTo });

    queryParams = this.queryService.queryToUrlQueryParams(query);

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
  }

  public dateLastThreeMonthsSelected(): void {
    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth();
    const daysInMonth = new Date(year, month + 1, 0).getDate();

    const dateFrom = this.dateToSimpleIsoString(new Date(year, month - 2, 1));
    const dateTo = this.dateToSimpleIsoString(new Date(year, month, daysInMonth));

    let queryParams = { ...this.route.snapshot.queryParams };
    const query = this.queryService.queryParamsToQuery(queryParams);

    query.filterBy = query.filterBy.filter((filter) => filter.key !== 'date');
    query.filterBy.push({ key: 'date', operator: FilterOperator.GreaterThanOrEqual, value: dateFrom });
    query.filterBy.push({ key: 'date', operator: FilterOperator.LessThanOrEqual, value: dateTo });

    queryParams = this.queryService.queryToUrlQueryParams(query);

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
  }

  public dateFromSelected(fromDate: Date): void {
    let queryParams = { ...this.route.snapshot.queryParams };
    const date = this.dateToSimpleIsoString(fromDate);
    const query = this.queryService.queryParamsToQuery(queryParams);
    query.filterBy = query.filterBy.filter((filter) => filter.key !== 'date' || filter.operator !== FilterOperator.GreaterThanOrEqual);
    query.filterBy.push({ key: 'date', operator: FilterOperator.GreaterThanOrEqual, value: date });

    queryParams = this.queryService.queryToUrlQueryParams(query);

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
  }

  public dateToSelected(toDate: Date): void {
    let queryParams = { ...this.route.snapshot.queryParams };
    const date = this.dateToSimpleIsoString(toDate);
    const query = this.queryService.queryParamsToQuery(queryParams);
    query.filterBy = query.filterBy.filter((filter) => filter.key !== 'date' || filter.operator !== FilterOperator.LessThanOrEqual);
    query.filterBy.push({ key: 'date', operator: FilterOperator.LessThanOrEqual, value: date });

    queryParams = this.queryService.queryToUrlQueryParams(query);

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      skipLocationChange: false,
    });
  }

  public clearDateSelection(): void {
    const queryParams = { ...this.route.snapshot.queryParams };
    delete queryParams.date;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      skipLocationChange: false,
    });
  }

  public dateRangeTooltip(dateFrom: Date, dateTo: Date): string {
    if (!dateFrom && !dateTo) {
      return 'All dates';
    }
    if (dateFrom && !dateTo) {
      return `From ${this.datePipe.transform(dateFrom)}`;
    }
    if (!dateFrom && dateTo) {
      return `To ${this.datePipe.transform(dateTo)}`;
    }
    return `${this.datePipe.transform(dateFrom)} - ${this.datePipe.transform(dateTo)}`;
  }

  public sumSelectedTransactions(): number {
    if (this.selectedTransaction.length) {
      return this.selectedTransaction.reduce((sum, transaction) => sum + transaction.amount, 0);
    } else {
      return null;
    }
  }

  public selectedOutflowTransactions(): { count: number, sum: number } {
    const selectedIncomeTransactions = this.selectedTransaction
      .filter((transaction) => transaction.amount < 0);
    const count = selectedIncomeTransactions.length;
    const sum = selectedIncomeTransactions.reduce((s, transaction) => s + transaction.amount, 0);
    return { count, sum };
  }

  public selectedInflowTransactions(): { count: number, sum: number } {
    const selectedIncomeTransactions = this.selectedTransaction
      .filter((transaction) => transaction.amount >= 0);
    const count = selectedIncomeTransactions.length;
    const sum = selectedIncomeTransactions.reduce((s, transaction) => s + transaction.amount, 0);
    return { count, sum };
  }

  public calendarIcon(dateFrom: Date, dateTo: Date): string {
    if (!dateFrom && !dateTo) {
      return 'calendar-blank';
    }

    if (dateFrom && dateTo) {
      const now = new Date();
      const year = now.getFullYear();
      const month = now.getMonth();
      const daysInMonth = new Date(year, month + 1, 0).getDate();
      if (dateFrom.getMonth() === month && dateFrom.getDate() === 1
        && dateTo.getMonth() === month && dateTo.getDate() === daysInMonth) {
        return 'calendar-today';
      }
      if (dateFrom.getMonth() === month - 2 && dateFrom.getDate() === 1
        && dateTo.getMonth() === month && dateTo.getDate() === daysInMonth) {
        return 'calendar-range';
      }
    }

    return 'calendar';
  }

  private resetSearchForm(): void {
    if (this.searchInput && this.searchInput.nativeElement) {
      this.searchInput.nativeElement.value = null;
    }
    this.searchControl.patchValue(null);
  }

  private dateToSimpleIsoString(date: Date): string {
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    return `${date.getFullYear()}-${month}-${day}`;
  }
}
