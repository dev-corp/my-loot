import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionsPageComponent } from './transactions-page.component';


const routes: Routes = [
  { path: 'all', component: TransactionsPageComponent },
  { path: 'account/:account.id', component: TransactionsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransactionsPageRoutingModule {}
