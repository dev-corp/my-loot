import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import { IState } from 'src/app/+state';
import { TransactionService } from 'src/app/services';
import { ITransactionPageFeatureState } from './transactions-page.interface';
import { TransactionsPageActions } from './transactions-page.actions';
import { FromTransactionPageState } from './transactions-page.selectors';
import { IQuery } from 'shared/search';


@Injectable()
export class TransactionsPageEffects {

  onSetQuery$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(TransactionsPageActions.setQuery),
      map(() => TransactionsPageActions.loadTransactions()),
    ),
  );

  onLoadTransaction$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(TransactionsPageActions.loadTransactions),
      withLatestFrom(this.store.select<IQuery>(FromTransactionPageState.selectQuery)),
      mergeMap(([action, query]) => this.transactionService.queryTransactions(query)
        .pipe(
          map((result) => TransactionsPageActions.loadTransactionsSuccess({
            transactions: result.transactions,
            query: result.query,
            total: result.total,
          })),
          catchError(() => of(TransactionsPageActions.loadTransactionsFail())),
        ),
      ),
    ),
  );

  constructor(private actions$: Actions,
              private store: Store<IState | ITransactionPageFeatureState>,
              private transactionService: TransactionService) {
  }
}
