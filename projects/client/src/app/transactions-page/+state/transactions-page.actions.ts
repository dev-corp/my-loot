/* tslint:disable:typedef */
import { createAction, props } from '@ngrx/store';
import { IQuery } from 'shared/search';
import { Account, Transaction } from 'shared/models';


export enum TransactionsPageActionsTypes {
  SetQuery = '[Transactions Page] Set Query', // Fires LoadTransactions in effect
  LoadTransactions = '[Transactions Page] Load Transactions',
  LoadTransactionsSuccess = '[Transactions Page] Load Transactions Success',
  LoadTransactionsFail = '[Transactions Page] Load Transactions Fail',
}

export class TransactionsPageActions {
  public static setQuery = createAction<TransactionsPageActionsTypes.SetQuery, { query: IQuery }>(
    TransactionsPageActionsTypes.SetQuery,
    props<{ query: IQuery }>(),
  );

  /**
   * Use for loading and reloading transactions
   */
  public static loadTransactions = createAction<TransactionsPageActionsTypes.LoadTransactions>(
    TransactionsPageActionsTypes.LoadTransactions,
  );

  public static loadTransactionsSuccess =
    createAction<TransactionsPageActionsTypes.LoadTransactionsSuccess, { transactions: Transaction[], query: IQuery, total: number }>(
      TransactionsPageActionsTypes.LoadTransactionsSuccess,
      props<{ transactions: Transaction[], query: IQuery, total: number }>(),
    );

  public static loadTransactionsFail = createAction<TransactionsPageActionsTypes.LoadTransactionsFail>(
    TransactionsPageActionsTypes.LoadTransactionsFail,
  );

}
