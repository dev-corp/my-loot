import { Action, createReducer, on } from '@ngrx/store';
import { ITransactionsPageState } from './transactions-page.interface';
import { TransactionsPageActions } from './transactions-page.actions';


const transactionPageInitialState: ITransactionsPageState = {
  query: null,
  transactions: [],
  total: null,
  isLoading: false,
};

const _transactionPageReducer = createReducer(
  transactionPageInitialState,
  on(TransactionsPageActions.setQuery, (state, { query }) => ({ ...state, ...{ query, isLoading: true } })),
  on(TransactionsPageActions.loadTransactions, (state) => ({ ...state, ...{ isLoading: true } })),
  on(TransactionsPageActions.loadTransactionsSuccess, (state, { transactions, query, total }) => ({
    ...state, ...{ transactions, total, query, isLoading: false },
  })),
  on(TransactionsPageActions.loadTransactionsFail, (state) => ({ ...state, ...{ transactions: [], isLoading: false } })),
);

export function transactionPageReducer(state: ITransactionsPageState | undefined, action: Action): ITransactionsPageState {
  return _transactionPageReducer(state, action);
}
