import { getSelectors } from '@ngrx/router-store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { Account, Transaction } from 'shared/models';
import { IState } from 'src/app/+state';
import { FilterOperator, IQuery } from 'shared/search';
import { ITransactionsPageState, IRootAndTransactionsFeatureState } from './transactions-page.interface';
import { FromAppState } from 'src/app/+state/app';


const selectRouter = (state: IState) => state.router;
const selectFeature = (state: IRootAndTransactionsFeatureState) => state.transactionsPage;


export class FromTransactionPageState {

  public static selectState: MemoizedSelector<IRootAndTransactionsFeatureState, ITransactionsPageState> = createSelector(
    selectFeature,
    (state) => state,
  );

  public static selectTransactions: MemoizedSelector<IRootAndTransactionsFeatureState, Transaction[]> = createSelector(
    selectFeature,
    (state) => state.transactions,
  );

  public static selectTotal: MemoizedSelector<IRootAndTransactionsFeatureState, number> = createSelector(
    selectFeature,
    (state) => state.total,
  );

  public static selectQuery: MemoizedSelector<IRootAndTransactionsFeatureState, IQuery> = createSelector(
    selectFeature,
    (state) => state.query,
  );

  public static selectCurrentAccount: MemoizedSelector<IRootAndTransactionsFeatureState, Account> = createSelector(
    selectFeature,
    FromAppState.selectAccounts,
    getSelectors(selectRouter).selectUrl,
    getSelectors(selectRouter).selectRouteParams,
    (state, accounts, url, params) =>
      url.includes('/transactions') ? accounts.find(((account) => account.id === params['account.id'])) || null : null,
  );

  public static selectQueryDateFrom: MemoizedSelector<IRootAndTransactionsFeatureState, Date> = createSelector(
    selectFeature,
    FromTransactionPageState.selectQuery,
    (state, query) => {
      if (query) {
        const dateFilter = query.filterBy.find((filter) =>
          filter.key === 'date' && filter.operator === FilterOperator.GreaterThanOrEqual);
        if (dateFilter) {
          return new Date(dateFilter.value);
        }
      }
      return null;
    },
  );

  public static selectQueryDateTo: MemoizedSelector<IRootAndTransactionsFeatureState, Date> = createSelector(
    selectFeature,
    FromTransactionPageState.selectQuery,
    (state, query) => {
      if (query) {
        const dateFilter = query.filterBy.find((filter) =>
          filter.key === 'date' && filter.operator === FilterOperator.LessThanOrEqual);
        if (dateFilter) {
          return new Date(dateFilter.value);
        }
      }
      return null;
    },
  );

}
