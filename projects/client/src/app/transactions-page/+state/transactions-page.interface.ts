import { Transaction } from 'shared/models';
import { IQuery } from 'shared/search';
import { IState } from 'src/app/+state';


export interface ITransactionPageFeatureState {
  transactionsPage: ITransactionsPageState;
}

export interface ITransactionsPageState {
  transactions: Transaction[];
  query: IQuery;
  total: number;
  isLoading: boolean;
}

export type IRootAndTransactionsFeatureState = IState & ITransactionPageFeatureState
