import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GenericDialogButtons } from './generic-dialog-buttons.enum';
import { IGenericDialogOptions } from './generic-dialog-options.interface';


@Component({
  selector: 'ml-generic-dialog',
  templateUrl: './generic-dialog.component.html',
  styleUrls: ['./generic-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericDialogComponent implements OnInit {

  public showOk: boolean;
  public showYes: boolean;
  public showNo: boolean;
  public showConfirm: boolean;
  public showCancel: boolean;
  public showAgree: boolean;
  public showDisagree: boolean;

  public readonly message: string;
  public readonly description: string;

  constructor(private dialogRef: MatDialogRef<GenericDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: IGenericDialogOptions) {
    this.markButtonsToShow(data.buttons);
    this.message = data.message;
    this.description = data.description || null;
  }

  ngOnInit(): void {
  }

  private markButtonsToShow(genericDialogButtons: GenericDialogButtons): void {
    if (genericDialogButtons.includes('ok')) { this.showOk = true; }
    if (genericDialogButtons.includes('yes')) { this.showYes = true; }
    if (genericDialogButtons.includes('no')) { this.showNo = true; }
    if (genericDialogButtons.includes('confirm')) { this.showConfirm = true; }
    if (genericDialogButtons.includes('cancel')) { this.showCancel = true; }
    if (genericDialogButtons.includes('agree')) { this.showAgree = true; }
    if (genericDialogButtons.includes('disagree')) { this.showDisagree = true; }
  }

}
