import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { GenericDialogComponent } from './generic-dialog.component';


@NgModule({
  declarations: [GenericDialogComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
  ],
  entryComponents: [GenericDialogComponent],
  exports: [GenericDialogComponent],
})
export class GenericDialogModule {}
