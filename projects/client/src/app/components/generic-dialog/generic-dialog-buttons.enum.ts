export enum GenericDialogButtons {
  Ok = 'ok',
  OkCancel = 'ok-cancel',
  YesCancel = 'yes-cancel',
  YesNo = 'yes-no',
  ConfirmCancel = 'confirm-cancel',
  YesNoCancel = 'yes-no-cancel',
  AgreeDisagree = 'agree-disagree',
}
