import { GenericDialogButtons } from './generic-dialog-buttons.enum';


export interface IGenericDialogOptions {
  buttons: GenericDialogButtons;
  message: string;
  description?: string;
}
