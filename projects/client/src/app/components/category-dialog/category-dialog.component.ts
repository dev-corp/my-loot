import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Category } from 'shared/models';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { improperTypeValidator } from 'src/app/validators';
import { Observable } from 'rxjs';
import { NgUnsubscribe } from '@devcorp-libs/ng-unsubscribe';
import { FromAppState } from 'src/app/+state/app';
import { Store } from '@ngrx/store';
import { StoreUtil } from '@devcorp-libs/store-util';
import { IState } from 'src/app/+state';
import { map } from 'rxjs/operators';
import { AutocompleteUtilService } from 'src/app/services';


export interface CategoryDialogData {
  type: 'parent' | 'child';
  category?: Category;
}


@Component({
  selector: 'ml-category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryDialogComponent extends NgUnsubscribe {

  public categoryForm: FormGroup = new FormGroup({
    parentCategory: new FormControl(null, [Validators.required, improperTypeValidator(Category)]),
    name: new FormControl(null, [Validators.required]),
  });

  public categoryToEdit: Category;

  public filteredParentCategories$: Observable<Category[]>;

  private readonly parentCategories$: Observable<Category[]>;

  constructor(private dialogRef: MatDialogRef<CategoryDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: CategoryDialogData,
              private store: Store<IState>,
              private autocompleteUtil: AutocompleteUtilService) {
    super();
    this.parentCategories$ = StoreUtil.select(this.store, FromAppState.selectParentCategories, this.ngUnsubscribe)
      .pipe(map((categories) => categories.filter((category) => !category.isSystem)));
    this.filteredParentCategories$ = this.autocompleteUtil.filterAutocompleteOptions<Category>(
      this.categoryForm.controls.parentCategory, this.parentCategories$,
      this.autocompleteUtil.childrenCategoryAutocompleteMapper, this.ngUnsubscribe);

    this.dialogRef._containerInstance._config.width = '300px';
    if (this.data && this.data.category) {
      this.categoryToEdit = this.data.category;
      this.categoryForm.patchValue(this.categoryToEdit);
    }
    if (!this.isChild()) {
      this.categoryForm.controls.parentCategory.setValidators([improperTypeValidator(Category)]);
    }
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public save(): void {
    if (this.categoryForm.invalid) {
      return;
    }

    let category;
    if (this.isEditDialog()) {
      category = new Category({ ...this.categoryToEdit, ...this.categoryForm.value });
    } else {
      category = new Category(this.categoryForm.value);
    }
    this.dialogRef.close(category);
  }

  public displayCategoryFn(category?: Category): string | undefined {
    return category ? category.fullName() : undefined;
  }

  public createParent(): void {
    if (typeof this.categoryForm.value.parentCategory === 'string') {
      const name = this.categoryForm.value.parentCategory;
      const parentCategory = new Category({ name, parentCategory: null });
      this.categoryForm.patchValue({ parentCategory });
    }
  }

  public isEditDialog(): boolean {
    return this.categoryToEdit && !!this.categoryToEdit.id;
  }

  public isChild(): boolean {
    return this.data && this.data.type === 'child' || !this.data; // defaults to child
  }

  public showAddParentButton(): boolean {
    return this.categoryForm.controls.parentCategory.value && typeof this.categoryForm.controls.parentCategory.value === 'string';
  }
}
