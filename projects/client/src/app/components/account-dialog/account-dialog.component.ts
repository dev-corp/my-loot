import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Account } from 'shared/models';


@Component({
  selector: 'ml-account-dialog',
  templateUrl: './account-dialog.component.html',
  styleUrls: ['./account-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountDialogComponent {

  public accountForm: FormGroup = new FormGroup({
    name: new FormControl(null, [Validators.required]),
    balance: new FormControl(null),
  });

  public accountToEdit: Account;

  constructor(private dialogRef: MatDialogRef<AccountDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Account) {
    if (data) {
      this.accountToEdit = data;
      this.accountForm.patchValue(this.accountToEdit);
      this.accountForm.controls.balance.clearValidators();
    }
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public closeAccount(): void {
    // TODO: Implement close account
    this.dialogRef.close();
  }

  public save(): void {
    if (!this.accountForm.valid) {
      return;
    }
    let account;
    if (this.isEditDialog()) {
      if (!isNaN(this.accountForm.value.balance)) {
        account = new Account({ ...this.accountToEdit, ...this.accountForm.value });
        account.currentBalance = this.accountForm.value.balance;
      }
    } else {
      account = new Account(this.accountForm.value);
      account.currentBalance = this.accountForm.value.balance;
    }
    this.dialogRef.close(account);
  }

  public isEditDialog(): boolean {
    return !!this.accountToEdit;
  }
}
