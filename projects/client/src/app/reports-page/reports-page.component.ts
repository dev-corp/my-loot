import { ChangeDetectionStrategy, Component } from '@angular/core';


@Component({
  selector: 'ml-reports-page',
  templateUrl: './reports-page.component.html',
  styleUrls: ['./reports-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportsPageComponent {

  constructor() { }

}
