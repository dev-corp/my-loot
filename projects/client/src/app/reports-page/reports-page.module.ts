import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsPageRoutingModule } from './reports-page-routing.module';
import { ReportsPageComponent } from './reports-page.component';


@NgModule({
  declarations: [ReportsPageComponent],
  imports: [
    CommonModule,
    ReportsPageRoutingModule,
  ],
})
export class ReportsPageModule {}
