import { ICacheState } from './cache';
import { IAppState } from 'src/app/+state/app';
import { RouterReducerState } from '@ngrx/router-store';


export interface IState {
  app: IAppState;
  cache: ICacheState;
  router: RouterReducerState;
}
