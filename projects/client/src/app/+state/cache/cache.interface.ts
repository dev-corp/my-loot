export interface ICache<T = any> {
  data: T;
  timestamp: number;
}

export interface ICacheState {
  [key: string]: ICache;
}
