import { createSelector, MemoizedSelectorWithProps } from '@ngrx/store';

import { ICache } from './cache.interface';
import { IState } from '../state.interface';


const selectFeature = (state: IState) => state.cache;

export class FromCacheState {

  public static selectCache: MemoizedSelectorWithProps<IState, { key: string }, ICache<any>> = createSelector(
    selectFeature,
    (state, props) => state[props.key] || null,
  );

}
