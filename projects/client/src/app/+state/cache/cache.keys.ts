export enum CacheKeys {
  AllPayee = 'allPayee',
  AllCategories = 'allCategories',
  AllSubCategories = 'allSubCategories',
  AllAccounts = 'allAccounts',
}
