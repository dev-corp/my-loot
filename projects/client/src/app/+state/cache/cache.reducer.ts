import { Action, createReducer, on } from '@ngrx/store';
import { ICacheState } from './cache.interface';
import { CacheActions } from './cache.actions';


const cacheInitialState: ICacheState = {};

const _cacheStateReducer = createReducer(
  cacheInitialState,
  on(CacheActions.cacheData, (state, { key, data }) => ({ ...state, ...{ [key]: { data, timestamp: Date.now() } } })),
  on(CacheActions.cleanDataCache, (state, { key }) => ({ ...state, ...{ [key]: null } })),
  on(CacheActions.removeAllCache, (state) => ({})),
);

export function cacheStateReducer(state: ICacheState | undefined, action: Action): ICacheState {
  return _cacheStateReducer(state, action);
}
