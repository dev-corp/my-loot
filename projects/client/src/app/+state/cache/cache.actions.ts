/* tslint:disable:typedef */
import { createAction, props } from '@ngrx/store';

import { CacheKeys } from './cache.keys';


export enum CacheActionTypes {
  CacheData = '[Cache] Cache Data',
  CleanDataCache = '[Cache] Clean Data Cache',
  RemoveAllCache = '[Cache] Remove All Cache',
}

export class CacheActions {
  public static cacheData = createAction<CacheActionTypes.CacheData, { key: string, data: any }>(
    CacheActionTypes.CacheData,
    props<{ key: string, data: any }>(),
  );

  public static cleanDataCache = createAction<CacheActionTypes.CleanDataCache, { key: CacheKeys | string }>(
    CacheActionTypes.CleanDataCache,
    props<{ key: CacheKeys | string }>(),
  );

  public static removeAllCache = createAction<CacheActionTypes.RemoveAllCache>(
    CacheActionTypes.RemoveAllCache,
  );
}

