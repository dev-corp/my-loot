import { Action, createReducer, on } from '@ngrx/store';
import { IAppState } from './app.interface';
import { AppActions } from './app.actions';


const appInitialState: IAppState = {
  accounts: [],
  categories: [],
  payees: [],
  isLoading: 0,
};

const _appStateReducer = createReducer(
  appInitialState,
  on(AppActions.loadAll, (state) => ({ ...state })),
  // Accounts
  on(AppActions.loadAccounts, (state) => ({ ...state, ...{ isLoading: state.isLoading + 1 } })),
  on(AppActions.loadAccountsSuccess, (state, { accounts }) => ({ ...state, ...{ accounts, isLoading: state.isLoading - 1 } })),
  on(AppActions.loadAccountsFailed, (state) => ({ ...state, ...{ isLoading: state.isLoading - 1 } })),
  // Categories
  on(AppActions.loadCategories, (state) => ({ ...state, ...{ isLoading: state.isLoading + 1 } })),
  on(AppActions.loadCategoriesSuccess, (state, { categories }) => ({
    ...state, ...{ categories, isLoading: state.isLoading - 1 },
  })),
  on(AppActions.loadCategoriesFailed, (state) => ({ ...state, ...{ isLoading: state.isLoading - 1 } })),
  // Payees
  on(AppActions.loadPayees, (state) => ({ ...state, ...{ isLoading: state.isLoading + 1 } })),
  on(AppActions.loadPayeesSuccess, (state, { payees }) => ({ ...state, ...{ payees } })),
  on(AppActions.loadPayeesFailed, (state) => ({ ...state, ...{ isLoading: state.isLoading - 1 } })),
);

export function appStateReducer(state: IAppState | undefined, action: Action): IAppState {
  return _appStateReducer(state, action);
}
