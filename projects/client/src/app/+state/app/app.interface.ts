import { Account, Category, Payee } from 'shared/models';


export interface IAppState {
  accounts: Account[];
  categories: Category[];
  payees: Payee[];
  isLoading: number;
}
