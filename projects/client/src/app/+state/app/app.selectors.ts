import { createSelector, MemoizedSelector } from '@ngrx/store';
import { Account, Category, Payee } from 'shared/models';
import { CategoryModelUtil } from 'shared/models/utils';
import { IState } from '../state.interface';


const selectFeature = (state: IState) => state.app;

export class FromAppState {

  public static selectAccounts: MemoizedSelector<IState, Account[]> = createSelector(
    selectFeature,
    (state) => state.accounts,
  );

  public static selectCategories: MemoizedSelector<IState, Category[]> = createSelector(
    selectFeature,
    (state) => state.categories || [],
  );

  public static selectChildrenCategories: MemoizedSelector<IState, Category[]> = createSelector(
    selectFeature,
    (state) => state && state.categories && CategoryModelUtil.extractChildren(state.categories) || [],
  );

  public static selectParentCategories: MemoizedSelector<IState, Category[]> = createSelector(
    selectFeature,
    (state) => state && state.categories && CategoryModelUtil.extractParents(state.categories) || [],
  );

  public static selectPayees: MemoizedSelector<IState, Payee[]> = createSelector(
    selectFeature,
    (state) => state.payees,
  );

  public static selectSearchableItems: MemoizedSelector<IState, (Account | Category | Payee)[]> = createSelector(
    selectFeature,
    (state) => state.accounts
      .concat(CategoryModelUtil.flatCategoriesFromParent(state.categories))
      .concat(state.payees),
  );
}
