import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { AccountService, CategoryService, PayeeService } from 'src/app/services';
import { IState } from '../state.interface';
import { AppActions } from './app.actions';
import { Category, Payee } from 'shared/models';


function categoryNameSortComparer(a: Category, b: Category): number {
  return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
}

function payeeNameSortComparer(a: Payee, b: Payee): number {
  return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
}

@Injectable()
export class AppEffects {

  onLoadAll$: Observable<Action> = createEffect(() =>
      this.actions$.pipe(
        ofType(AppActions.loadAll),
        mergeMap(() => of(
          AppActions.loadAccounts(),
          AppActions.loadCategories(),
          AppActions.loadPayees(),
        )),
      ),
    // { dispatch: false },
  );

  onLoadAccounts$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(AppActions.loadAccounts),
      mergeMap(() => this.accountService.getAllAccounts(true)
        .pipe(
          map((accounts) => AppActions.loadAccountsSuccess({ accounts })),
          catchError(() => of(AppActions.loadAccountsFailed())),
        ),
      ),
    ),
  );

  onLoadCategories$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(AppActions.loadCategories),
      mergeMap(() => this.categoryService.getAllCategories()
        .pipe(
          // Sort parents
          map((categories) => categories.sort(categoryNameSortComparer)),
          // Sort childrenCategories
          map((categories) => categories.map((parentCategory) => {
            parentCategory.childrenCategories.sort(categoryNameSortComparer);
            return parentCategory;
          })),
          map((categories) => AppActions.loadCategoriesSuccess({ categories })),
          catchError(() => of(AppActions.loadCategoriesFailed())),
        ),
      ),
    ),
  );

  onLoadPayees$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(AppActions.loadPayees),
      mergeMap(() => this.payeeService.getAllPayees()
        .pipe(
          map((payees) => payees.sort(payeeNameSortComparer)),
          map((payees) => AppActions.loadPayeesSuccess({ payees })),
          catchError(() => of(AppActions.loadPayeesFailed())),
        ),
      ),
    ),
  );

  constructor(private actions$: Actions,
              private store: Store<IState>,
              private accountService: AccountService,
              private categoryService: CategoryService,
              private payeeService: PayeeService) {
  }
}
