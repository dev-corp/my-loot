/* tslint:disable:typedef */
import { createAction, props } from '@ngrx/store';
import { Account, Category, Payee } from 'shared/models';


export enum AppActionTypes {
  LoadAll = '[App] Load All',
  // Accounts
  LoadAccounts = '[App] Load Accounts',
  LoadAccountsSuccess = '[App] Load Accounts Success',
  LoadAccountsFailed = '[App] Load Accounts Failed',
  // Categories
  LoadCategories = '[App] Load Categories',
  LoadCategoriesSuccess = '[App] Load Categories Success',
  LoadCategoriesFailed = '[App] Load Categories Failed',
  // Payees
  LoadPayees = '[App] Load Payees',
  LoadPayeesSuccess = '[App] Load Payees Success',
  LoadPayeesFailed = '[App] Load Payees Failed',
}

export class AppActions {
  public static loadAll = createAction<AppActionTypes.LoadAll>(AppActionTypes.LoadAll);

  // Accounts
  public static loadAccounts = createAction<AppActionTypes.LoadAccounts>(AppActionTypes.LoadAccounts);

  public static loadAccountsSuccess = createAction<AppActionTypes.LoadAccountsSuccess, { accounts: Account[] }>(
    AppActionTypes.LoadAccountsSuccess,
    props<{ accounts: Account[] }>(),
  );

  public static loadAccountsFailed = createAction<AppActionTypes.LoadAccountsFailed>(AppActionTypes.LoadAccountsFailed);

  // Categories
  public static loadCategories = createAction<AppActionTypes.LoadCategories>(AppActionTypes.LoadCategories);

  public static loadCategoriesSuccess = createAction<AppActionTypes.LoadCategoriesSuccess, { categories: Category[] }>(
    AppActionTypes.LoadCategoriesSuccess,
    props<{ categories: Category[] }>(),
  );

  public static loadCategoriesFailed = createAction<AppActionTypes.LoadCategoriesFailed>(AppActionTypes.LoadCategoriesFailed);

  // Payees
  public static loadPayees = createAction<AppActionTypes.LoadPayees>(AppActionTypes.LoadPayees);

  public static loadPayeesSuccess = createAction<AppActionTypes.LoadPayeesSuccess, { payees: Payee[] }>(
    AppActionTypes.LoadPayeesSuccess,
    props<{ payees: Payee[] }>(),
  );

  public static loadPayeesFailed = createAction<AppActionTypes.LoadPayeesFailed>(AppActionTypes.LoadPayeesFailed);
}
