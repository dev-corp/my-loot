import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GenericDialogButtons, GenericDialogComponent, IGenericDialogOptions } from 'src/app/components';


@Component({
  selector: 'ml-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss'],
})
export class TestPageComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public openDialog(): void {
    const ref = this.dialog.open<GenericDialogComponent, IGenericDialogOptions, string>(GenericDialogComponent, {
      data: {
        buttons: GenericDialogButtons.YesNoCancel,
        message: 'Is this hello world?', description: 'Please answer this question',
      },
    });
    ref.afterClosed()
      .subscribe((reason) => console.log(reason));
  }
}
