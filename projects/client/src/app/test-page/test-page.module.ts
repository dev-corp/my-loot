import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GenericDialogModule } from 'src/app/components';
import { TestPageRoutingModule } from './test-page-routing.module';
import { TestPageComponent } from './test-page.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [TestPageComponent],
  imports: [
    CommonModule,
    TestPageRoutingModule,
    MatButtonModule,
    MatDialogModule,
    GenericDialogModule,
  ],
})
export class TestPageModule {}
