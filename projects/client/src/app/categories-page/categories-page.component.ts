import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { StoreUtil } from '@devcorp-libs/store-util';
import { NgUnsubscribe } from '@devcorp-libs/ng-unsubscribe';
import { AppActions, FromAppState } from 'src/app/+state/app';
import { IState } from 'src/app/+state';
import { Category } from 'shared/models';
import { CategoryDialogComponent, CategoryDialogData } from 'src/app/components/category-dialog';
import { CategoryService } from 'src/app/services';


@Component({
  selector: 'ml-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesPageComponent extends NgUnsubscribe {

  public categoriesTree$: Observable<Category[]>;

  constructor(private store: Store<IState>,
              private dialog: MatDialog,
              private categoryService: CategoryService) {
    super();
    // TODO: sort by name
    this.categoriesTree$ = StoreUtil.select(this.store, FromAppState.selectCategories, this.ngUnsubscribe)
      .pipe(
        map((categories) => categories.filter((category) => !category.isSystem)),
      );

    this.store.dispatch(AppActions.loadCategories());
  }

  public createParentCategory(): void {
    const dialogRef = this.dialog.open<CategoryDialogComponent, CategoryDialogData, Category>(CategoryDialogComponent, {
      data: { type: 'parent' },
    });

    dialogRef.afterClosed()
      .pipe(
        filter((response) => response instanceof Category),
        switchMap((category) => this.categoryService.createCategory(category)),
      )
      .subscribe(() => {
        this.store.dispatch(AppActions.loadCategories());
      });
  }

  public createChildCategory(parentCat: Category = null): void {
    const category = new Category({ name: null, parentCategory: parentCat });

    const dialogRef = this.dialog.open<CategoryDialogComponent, CategoryDialogData, Category>(CategoryDialogComponent, {
      data: { type: 'child', category },
    });

    dialogRef.afterClosed()
      .pipe(
        filter((response) => response instanceof Category),
        switchMap((cat) => this.categoryService.createCategory(cat)),
      )
      .subscribe(() => {
        this.store.dispatch(AppActions.loadCategories());
      });
  }

  public editParentCategory(category: Category): void {
    const dialogRef = this.dialog.open<CategoryDialogComponent, CategoryDialogData, Category>(CategoryDialogComponent, {
      data: { type: 'parent', category },
    });

    dialogRef.afterClosed()
      .pipe(
        filter((response) => response instanceof Category),
        switchMap((cat) => this.categoryService.updateCategory(cat)),
      )
      .subscribe(() => {
        this.store.dispatch(AppActions.loadCategories());
      });
  }

  public editChildCategory(childCat: Category, parentCategory: Category): void {
    let category = childCat;
    if (!category.parentCategory) {
      category = new Category({ ...childCat, ...{ parentCategory } });
    }

    const dialogRef = this.dialog.open<CategoryDialogComponent, CategoryDialogData, Category>(CategoryDialogComponent, {
      data: { type: 'child', category },
    });

    dialogRef.afterClosed()
      .pipe(
        filter((response) => response instanceof Category),
        switchMap((cat) => this.categoryService.updateCategory(cat)),
      )
      .subscribe(() => {
        this.store.dispatch(AppActions.loadCategories());
      });
  }
}
