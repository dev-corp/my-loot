import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';

import { CategoriesPageRoutingModule } from './categories-page-routing.module';
import { CategoriesPageComponent } from './categories-page.component';
import { CategoryDialogModule } from 'src/app/components/category-dialog';


@NgModule({
  declarations: [CategoriesPageComponent],
    imports: [
        CommonModule,
        CategoriesPageRoutingModule,
        MatToolbarModule,
        MatListModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        CategoryDialogModule,
    ],
})
export class CategoriesPageModule {}
