import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { FilterOperator, IFilter } from 'shared/search';
import { StoreUtil } from '@devcorp-libs/store-util';
import { debounceTime, map, takeUntil } from 'rxjs/operators';
import { CurrencyPipe } from '@angular/common';
import { Account, Category, Payee } from 'shared/models';
import { FromAppState } from 'src/app/+state/app/app.selectors';
import { IState } from 'src/app/+state/state.interface';


export interface ITransactionSearchFilter extends IFilter {
  label: string;
  object?: any;
  class?: Function; // tslint:disable-line:ban-types
}

export type SearchableItem = Account | Category | Payee;

@Injectable({
  providedIn: 'root',
})
export class TransactionSearchService {

  constructor(private store: Store<IState>,
              private currencyPipe: CurrencyPipe) {
  }

  public transactionSearchableItems(unsubscribe: Observable<any>): Observable<ITransactionSearchFilter[]> {
    return StoreUtil.select(this.store, FromAppState.selectSearchableItems).pipe(
      takeUntil(unsubscribe),
      debounceTime(100),
      map((items) =>
        items.map((item) => {
          let key = `${item.constructor.name.toLowerCase()}.name`;
          if (item instanceof Category && item.isParent) {
            key = `${item.constructor.name.toLowerCase()}.parentCategory.name`;
          }
          return {
            operator: FilterOperator.Equal,
            key,
            value: item.name,
            label: this.classToLabel(item),
            object: item,
            class: item.constructor,
          } as ITransactionSearchFilter;
        }),
      ),
    );
  }

  /**
   * Factory for transaction search autocomplete mapper.
   * Filter available options and add amount and memo
   */
  public filterOptionMapperFactory(): (value: string | ITransactionSearchFilter, options: ITransactionSearchFilter[]) => ITransactionSearchFilter[] {
    return (value, options) => {
      if (value === null) {
        return options;
      }
      let parsedValue: string;
      if (typeof value === 'string') {
        parsedValue = value;
      } else {
        parsedValue = value.label;
      }
      const filteredOptions = options.filter((item) => item.label.toLowerCase().includes(parsedValue.toLowerCase()));

      // Add filter for memo
      if (typeof value === 'string' && value.length > 0) {
        const parsedFilter = this.parseStringToFilter(value);
        if (parsedFilter) {
          filteredOptions.push(parsedFilter);
        } else {
          filteredOptions.push({
            // TODO: Translate label
            label: `memo: ${parsedValue}`,
            key: 'memo',
            value: parsedValue,
            operator: FilterOperator.Contains,
          });
        }
      }

      // Add filter option for amount
      if (!isNaN(parseFloat(parsedValue))) {
        const operators = [FilterOperator.Equal, FilterOperator.LessThan,
          FilterOperator.GreaterThan, FilterOperator.LessThanOrEqual, FilterOperator.GreaterThanOrEqual];
        operators.forEach((operator) => {
          filteredOptions.push({
            // TODO: Translate label
            label: `amount ${operator === FilterOperator.Equal ? '=' : operator} ${this.currencyPipe.transform(+parsedValue)}`,
            key: 'amount',
            value: +parsedValue,
            operator,
          });
        });
      }

      return filteredOptions;
    };
  }

  private parseStringToFilter(inputValue: string): ITransactionSearchFilter {
    const regex = /[=:!<>@]+/g;
    const sections = inputValue.split(regex);
    const operatorMatch = inputValue.match(regex);

    if (!sections || sections.length !== 2 || !operatorMatch || operatorMatch.length !== 1) {
      return null;
    }

    let operator = operatorMatch[0];
    if (operator === '=' || operator === ':') {
      operator = FilterOperator.Equal;
    }

    if (!Object.values<string>(FilterOperator).includes(operator)) {
      return null;
    }

    const key = sections[0].trim();
    const value = String(sections[1].trim());

    return {
      key,
      operator,
      value,
      label: `manual: ${key} ${operator} ${value}`,
    };
  }

  private classToLabel(item: SearchableItem): string {
    // TODO: Make it translatable
    return item.constructor.name.toLowerCase() + ': ' + item.name;
  }
}
