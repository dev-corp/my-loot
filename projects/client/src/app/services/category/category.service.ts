import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'shared/models';
import { ICategory } from 'shared/interfaces';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class CategoryService {

  private readonly basePath: string = `${environment.apiUrl}/categories`;

  constructor(private http: HttpClient) {
  }

  public getAllCategories(): Observable<Category[]> {
    return this.http.get<ICategory[]>(`${this.basePath}`)
      .pipe(map((categories) => categories.map((category) => new Category(category))));
  }

  public createCategory(category: Category): Observable<Category> {
    return this.http.post<ICategory>(`${this.basePath}`, category)
      .pipe(map((cat) => new Category(cat)));
  }

  public updateCategory(category: Category): Observable<Category> {
    return this.http.put<ICategory>(`${this.basePath}/${category.id}`, category)
      .pipe(map((cat) => new Category(cat)));
  }
}
