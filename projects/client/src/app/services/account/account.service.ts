import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Account } from 'shared/models';
import { IAccount } from 'shared/interfaces';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class AccountService {

  private readonly basePath: string = `${environment.apiUrl}/accounts`;

  constructor(private http: HttpClient) {
  }

  public getAllAccounts(loadCurrentBalance: boolean): Observable<Account[]> {
    return this.http.get<IAccount[]>(`${this.basePath}`, { params: { loadCurrentBalance: loadCurrentBalance.toString() } })
      .pipe(map((accounts) => accounts.map((a) => new Account(a))));
  }

  public createAccount(account: Account): Observable<Account> {
    return this.http.post<IAccount>(`${this.basePath}`, account)
      .pipe(map((a) => new Account(a)));
  }

  public updateAccount(account: Account): Observable<Account> {
    return this.http.put<IAccount>(`${this.basePath}/${account.id}`, account)
      .pipe(map((a) => new Account(a)));
  }
}
