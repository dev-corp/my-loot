import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { IQuery, QueryUtility } from 'shared/search';
import { Params } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class QueryService {

  constructor() {
  }

  /**
   * Build http query params from IQuery object
   */
  public buildHttpQueryParams(query: IQuery): HttpParams {
    const params = QueryUtility.queryToQueryParams(query);
    let httpParams = new HttpParams();
    for (const key of Object.keys(params)) {
      httpParams = httpParams.append(key, params[key]);
    }
    return httpParams;
  }

  /**
   * Build query params from IQuery object
   */
  public queryToUrlQueryParams(query: IQuery): Params {
    return QueryUtility.queryToQueryParams(query);
  }

  public queryParamsToQuery(queryParams: Params): IQuery {
    return QueryUtility.queryParamsToQuery(queryParams);
  }
}
