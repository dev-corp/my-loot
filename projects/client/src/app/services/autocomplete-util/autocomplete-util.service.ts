import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { combineLatest, Observable } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { Account, Category, Payee } from 'shared/models';


@Injectable({
  providedIn: 'root',
})
export class AutocompleteUtilService {

  constructor() { }


  public filterAutocompleteOptions<T>(control: AbstractControl,
                                      optionsSource$: Observable<T[]>,
                                      mapper: (value: string | T, options: T[]) => T[],
                                      unsubscribe?: Observable<void>): Observable<T[]> {
    const obs = combineLatest([control.valueChanges.pipe(startWith('')), optionsSource$])
      .pipe(
        map(([value, options]: [string | T, T[]]) => {
          return mapper(value, options);
        }),
      );

    if (unsubscribe) {
      obs.pipe(takeUntil(unsubscribe));
    }

    return obs;
  }

  public accountAutocompleteMapper(value: string | Account, options: Account[]): Account[] {
    if (value === null) {
      return options;
    }
    const parsedValue = value instanceof Account ? value.name : value;
    return options.filter((account) => account.name.toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase()));
  }

  public childrenCategoryAutocompleteMapper(value: string | Category, options: Category[]): Category[] {
    if (value === null) {
      return options;
    }
    const parsedValue = value instanceof Category ? value.fullName() : value;
    return options.filter((category) => category.fullName().toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase()));
  }

  public payeeAutocompleteMapper(value: string | Payee, options: Payee[]): Payee[] {
    if (value === null) {
      return options;
    }
    const parsedValue = value instanceof Payee ? value.name : value;
    return options.filter((payee) => payee.name.toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase()));
  }

  public searchItemsAutocompleteMapper(value: string | Account | Category | Payee,
                                       options: (Account | Category | Payee)[]): (Account | Category | Payee)[] {
    if (value === null) {
      return options;
    }

    let parsedValue: string;
    if (value instanceof Account) {
      parsedValue = value.name;
    } else if (value instanceof Category) {
      parsedValue = value.fullName();
    } else if (value instanceof Payee) {
      parsedValue = value.name;
    } else {
      parsedValue = value;
    }

    return options.filter((searchItem) => {
      if (searchItem instanceof Account) {
        return searchItem.name.toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase());
      } else if (searchItem instanceof Category) {
        return searchItem.fullName().toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase());
      } else if (searchItem instanceof Payee) {
        return searchItem.name.toLocaleLowerCase().includes(parsedValue.toLocaleLowerCase());
      } else {
        return false;
      }
    });
  }

}
