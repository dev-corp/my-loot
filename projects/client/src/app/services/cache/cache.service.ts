import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { CacheActions, CacheKeys, FromCacheState, ICache } from 'src/app/+state/cache';


@Injectable({
  providedIn: 'root',
})
export class CacheService {

  public static readonly DEFAULT_CACHE_TIME: number = 1000 * 60 * 5;
  public static CACHE_TIME: number;

  constructor(private store: Store<any>) {
  }

  public static getCacheTime(): number {
    return this.CACHE_TIME || this.DEFAULT_CACHE_TIME;
  }

  public cleanCache(...cacheKeys: (CacheKeys | string)[]): void {
    cacheKeys.forEach((key) => this.store.dispatch(CacheActions.cleanDataCache({ key })));
  }

  public useCache<T>(cacheKey: string, endpoint$: Observable<T>, cacheTime?: number): Observable<T> {
    const cacheSource$ = this.store.pipe(
      select(FromCacheState.selectCache, { key: cacheKey }),
    );

    return of(null).pipe(
      withLatestFrom(cacheSource$),
      switchMap(([_, latestState]: [null, ICache<T>]) => {
        if (!latestState || latestState.data === null || Date.now() - latestState.timestamp > (cacheTime || CacheService.getCacheTime())) {
          return endpoint$.pipe(
            tap((data) => this.store.dispatch(CacheActions.cacheData({ key: cacheKey, data }))),
          );
        } else {
          return of(latestState.data);
        }
      }),
    );
  }

}
