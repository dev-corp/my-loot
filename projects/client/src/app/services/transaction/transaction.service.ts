import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Transaction } from 'shared/models';
import { ITransaction } from 'shared/interfaces';
import { IQuery } from 'shared/search';
import { QueryService } from '../query/query.service';


@Injectable({
  providedIn: 'root',
})
export class TransactionService {

  private readonly basePath: string = `${environment.apiUrl}/transactions`;
  private readonly previousQueryKiller: Subject<void> = new Subject();

  constructor(private http: HttpClient,
              private queryService: QueryService) {
  }

  public queryTransactions(query: any): Observable<{ transactions: Transaction[], query: IQuery, total: number }> {
    const params = this.queryService.buildHttpQueryParams(query);
    // Kills previous request so the latest result is used
    this.previousQueryKiller.next();
    return this.http.get<{ transactions: ITransaction[], query: IQuery, total: number }>(`${this.basePath}`, { params })
      .pipe(
        takeUntil(this.previousQueryKiller),
        map((response) => ({
          transactions: response.transactions.map((transaction) => new Transaction(transaction)),
          query: response.query,
          total: response.total,
        })),
      );
  }

  public createTransaction(transaction: Transaction): Observable<Transaction> {
    return this.http.post<ITransaction>(`${this.basePath}`, transaction)
      .pipe(map((t) => new Transaction(t)));
  }

  public updateTransaction(transaction: Transaction): Observable<Transaction> {
    return this.http.put<ITransaction>(`${this.basePath}/${transaction.id}`, transaction)
      .pipe(map((t) => new Transaction(t)));
  }

  public updateMultipleTransactions(transactions: Transaction[]): Observable<Transaction[]> {
    return this.http.put<ITransaction[]>(`${this.basePath}`, transactions)
      .pipe(map((ts) => ts.map((t) => new Transaction(t))));
  }

  public deleteTransaction(transaction: Transaction): Observable<any> {
    return this.http.delete<any>(`${this.basePath}/${transaction.id}`);
  }

  public deleteMultipleTransactions(transactions: Transaction[]): Observable<any> {
    return this.http.request<any>('DELETE', `${this.basePath}`, {
      body: transactions,
    });
  }
}
