import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Payee } from 'shared/models';
import { IPayee } from 'shared/interfaces';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class PayeeService {

  private readonly basePath: string = `${environment.apiUrl}/payees`;

  constructor(private http: HttpClient) {
  }

  public getAllPayees(): Observable<Payee[]> {
    return this.http.get<IPayee[]>(`${this.basePath}`)
      .pipe(map((payees) => payees.map((a) => new Payee(a))));
  }
}
