import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayeeDisplayNamePipe } from './payee-display-name.pipe';


@NgModule({
  declarations: [PayeeDisplayNamePipe],
  imports: [CommonModule],
  exports: [PayeeDisplayNamePipe],
})
export class PayeeDisplayNamePipeModule {}
