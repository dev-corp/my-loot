import { Pipe, PipeTransform } from '@angular/core';
import { Payee } from 'shared/models';


@Pipe({
  name: 'payeeDisplayName',
})
export class PayeeDisplayNamePipe implements PipeTransform {

  public transform(payee: Payee, ...args: unknown[]): string {
    if (!payee) {
      return;
    }
    if (payee.isAccount) {
      // TODO: String need to be translatable
      return `${payee.name} (Transfer)`;
    }
    return payee.name;
  }

}
