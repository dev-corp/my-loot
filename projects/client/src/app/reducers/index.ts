import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';
import { environment } from 'src/environments/environment';
import { IState } from 'src/app/+state';
import { cacheStateReducer } from 'src/app/+state/cache';
import { appStateReducer } from 'src/app/+state/app';


export const reducers: ActionReducerMap<IState> = {
  app: appStateReducer,
  cache: cacheStateReducer,
  router: routerReducer,
};


export const metaReducers: MetaReducer<IState>[] = !environment.production ? [] : [];
