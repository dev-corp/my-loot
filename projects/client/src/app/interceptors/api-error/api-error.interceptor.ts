import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class ApiErrorInterceptor implements HttpInterceptor {

  constructor(private snackBar: MatSnackBar) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        let message: string;

        // TODO: Translate errors message
        if (error.status === 0) {
          message = 'Unknown error';
        } else if (error.status === 400) {
          message = 'Bad request';
        } else if (error.status === 404) {
          message = 'Not found';
        } else if (error.status === 409) {
          message = 'Conflict';
        } else if (error.status === 500) {
          message = 'Something went wrong';
        }

        console.log(error.message);

        if (message) {
          this.snackBar.open(message, 'Okay', {
            duration: 5000,
          });
        }
        return throwError(error);
      }),
    );
  }

}
