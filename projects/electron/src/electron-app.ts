import * as Electron from 'electron';
import * as url from 'url';
import * as path from 'path';
import { ConnectionOptions } from 'typeorm';
import { MyLootApi } from 'src/my-loot-api';


export class ElectronApp {

  private readonly _app: Electron.App;
  private readonly _api: MyLootApi;
  private _window: Electron.BrowserWindow;
  private readonly _frontendUrl: string;

  constructor(connectionOptions: ConnectionOptions, frontendPath: string) {
    this._frontendUrl = url.format({
      pathname: path.join(__dirname, frontendPath),
      protocol: 'file:',
      slashes: true,
    });

    this._api = new MyLootApi(connectionOptions);

    this._app = Electron.app;

    this._app.on('ready', () => this.onAppReady());
    this._app.on('window-all-closed', () => this.onAppWindowAllClosed());
    this._app.on('activate', () => this.onAppActivate());
  }

  // App Listeners

  private onAppReady(): void {
    this.createWindow();
  }

  private onAppWindowAllClosed(): void {
    if (process.platform !== 'darwin') {
      this._app.quit();
    }
  }

  private onAppActivate(): void {
    if (this._window === null) {
      this.createWindow();
    }
  }

  // Others

  private createWindow(): void {
    this._window = new Electron.BrowserWindow({
      width: 1680,
      height: 1050,
      webPreferences: {
        nodeIntegration: true,
      },
    });

    this.loadClient(this._window);

    // Dev tools
    if (process.env.NODE_ENV === 'development') {
      this._window.webContents.openDevTools();
    }

    this._window.on('closed', () => { this._window = null; });

    this._window.webContents.on('did-fail-load', () => {
      this.loadClient(this._window);
    });
  }

  private loadClient(window: Electron.BrowserWindow): Promise<void> {
    return window.loadURL(this._frontendUrl);
  }
}
