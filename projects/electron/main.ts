import { ElectronApp } from './src';
import { ConnectionOptions } from 'typeorm';
import { AccountEntity, CategoryEntity, PayeeEntity, TransactionEntity } from 'database/entities';


// TODO: This is temporary solution until creating a mechanism for electron to setup the database
const database = 'sqlite';
const ormConfig: ConnectionOptions[] = require('../../ormconfig.json');
let connectionOptions = { ...ormConfig.find((cfg) => cfg.name === database) };

if (!connectionOptions) {
  throw new Error(`Could not find ${database} configuration`);
}

// Need to remove name to make this connection default
delete connectionOptions.name;

const databaseSetup = {
  // Entities need to be setup outside of ormconfig.json otherwise electron fails in loading api
  entities: [
    AccountEntity,
    CategoryEntity,
    PayeeEntity,
    TransactionEntity,
  ],
  migrations: [], // TODO: Need to handle this in future
};

connectionOptions = { ...{}, ...connectionOptions, ...databaseSetup };

// TODO: frontend path to be passed as arguments
const app = new ElectronApp(connectionOptions, './client/index.html');
