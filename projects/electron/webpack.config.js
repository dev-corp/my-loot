'use strict';
const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const exec = require('child_process').exec;


const tsConfigPath = 'projects/electron/tsconfig.electron.json';
const outputPath = 'dist/desktop';

module.exports = {
  target: 'electron-main',
  mode: 'development',
  devtool: 'inline-source-map',
  entry: './projects/electron/main.ts',
  node: {
    __dirname: false
  },
  module: {
    rules: [
      {
        // TypeScript
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: tsConfigPath
        }
      },
    ]
  },
  resolve: {
    plugins: [new TsconfigPathsPlugin({ configFile: tsConfigPath })],
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'electron.js',
    path: path.resolve(__dirname, '../../', outputPath),
  },
  externals: [nodeExternals()],
  plugins: [
    {
      apply: (compiler) => {
        compiler.hooks.afterEmit.tap('Build client', (compilation) => {
          // Build client required for electron
          exec(`npm run build:client -- --output-path=${outputPath}/client`, (err, stdout, stderr) => {
            if (stdout) process.stdout.write(stdout);
            if (stderr) process.stderr.write(stderr);
          });
        });
      }
    },
    {
      apply: (compiler) => {
        compiler.hooks.afterEmit.tap('Build entities and migrations', (compilation) => {
          // Build migrations and entities
          exec(`tsc --project projects/database/tsconfig.json --outDir ${outputPath} && rimraf ${outputPath}/shared`, (err, stdout, stderr) => {
            if (stdout) process.stdout.write(stdout);
            if (stderr) process.stderr.write(stderr);
          });
        });
      }
    }
  ]
};
