export enum SystemCategoryId {
  SystemMasterCategory = 'system_master_category',
  SystemIncome = 'system_income',
}

export enum SystemPayeeId {
  SystemStartingBalance = 'system_starting_balance',
  SystemAdjustBalance = 'system_adjust_balance',
}
