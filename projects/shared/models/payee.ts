import { IPayee } from '../interfaces/payee.interface';


export class Payee implements IPayee {
  public id: string;
  public name: string;
  public isAccount?: boolean;
  public isSystem?: boolean;

  constructor(payeeObj?: IPayee) {
    if (payeeObj) {
      this.id = payeeObj.id;
      this.name = payeeObj.name;
      this.isAccount = payeeObj.isAccount;
      this.isSystem = payeeObj.isSystem;
    }
  }
}
