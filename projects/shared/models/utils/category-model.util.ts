import { Category } from '../category';


export class CategoryModelUtil {
  public static extractParents(categories: Category[]): Category[] {
    return categories.map((parentCategory) => this.parseParent(parentCategory));
  }

  public static extractChildren(categories: Category[]): Category[] {
    return categories
      .map((parentCategory) => parentCategory.childrenCategories
        .map((child) => {
          return new Category({ ...child, ...{ parentCategory: this.parseParent(parentCategory) } });
        }))
      .reduce((acc, val) => acc.concat(val), []);
  }

  public static flatCategoriesFromParent(parentCategories: Category[]): Category[] {
    let categories = [];
    parentCategories.map((pc) => {
      categories.push(this.parseParent(pc));
      categories = categories.concat(pc.childrenCategories);
    });
    return categories;
  }

  private static parseParent(category: Category): Category {
    return new Category({ ...category, ...{ childrenCategories: null } });
  }
}
