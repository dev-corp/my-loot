import { Account } from './account';
import { Payee } from './payee';
import { Category } from './category';
import { ITransaction } from '../interfaces/transaction.interface';


export class Transaction implements ITransaction {

  public id?: string;
  public date?: Date;
  public account?: Account;
  public payee?: Payee;
  public category?: Category;
  public memo?: string;
  public amount?: number;
  public transferTransaction: Transaction;

  constructor(transactionObj?: ITransaction) {
    if (transactionObj) {
      this.id = transactionObj.id;
      this.date = transactionObj.date;
      this.memo = transactionObj.memo;
      this.amount = transactionObj.amount;
      if (transactionObj.account !== undefined) {
        this.account = transactionObj.account ? new Account(transactionObj.account) : null;
      }
      if (transactionObj.payee !== undefined) {
        this.payee = transactionObj.payee ? new Payee(transactionObj.payee) : null;
      }
      if (transactionObj.category !== undefined) {
        this.category = transactionObj.category ? new Category(transactionObj.category) : null;
      }
      if (transactionObj.transferTransaction !== undefined) {
        this.transferTransaction = transactionObj.transferTransaction ? new Transaction(transactionObj.transferTransaction) : null;
      }
    }
  }

  public isTransfer(): boolean {
    return this.payee && this.payee.isAccount && !!this.transferTransaction;
  }
}
