import { ICategory } from '../interfaces/category.interface';


export class Category implements ICategory {
  public id: string;
  public name: string;
  public parentCategory?: Category;
  public childrenCategories?: Category[];
  public isSystem?: boolean;
  public isParent?: boolean;
  public isChild?: boolean;

  constructor(categoryObj?: ICategory) {
    if (categoryObj) {
      this.id = categoryObj.id;
      this.name = categoryObj.name;
      if (categoryObj.parentCategory !== undefined) {
        this.parentCategory = categoryObj.parentCategory ? new Category(categoryObj.parentCategory) : null;
      }
      if (categoryObj.childrenCategories !== undefined) {
        this.childrenCategories = categoryObj.childrenCategories
          ? categoryObj.childrenCategories.map((child) => child instanceof Category ? child : new Category(child))
          : [];
      }
      if (categoryObj.isParent !== undefined) {
        this.isParent = categoryObj.isParent;
      }
      if (categoryObj.isChild !== undefined) {
        this.isChild = categoryObj.isChild;
      }
      this.isSystem = categoryObj.isSystem;
    }
  }

  public fullName(): string {
    if (this.parentCategory) {
      return `${this.parentCategory.name}: ${this.name}`;
    }
    return this.name;
  }
}
