import { IAccount } from '../interfaces/account.interface';


export class Account implements IAccount {

  public id: string;
  public name: string;
  public currentBalance?: number;

  constructor(accountObj?: IAccount) {
    if (accountObj) {
      this.id = accountObj.id;
      this.name = accountObj.name;
      this.currentBalance = accountObj.currentBalance;
    }
  }
}
