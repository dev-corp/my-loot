import { ISort } from './sort.interface';
import { IFilter } from './filter.interface';


export interface IQuery {
  limit?: any;
  page?: any;
  sortBy?: ISort[];
  filterBy?: IFilter[];
}
