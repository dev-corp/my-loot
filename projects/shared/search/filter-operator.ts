export enum FilterOperator {
  Equal = '==',
  NotEqual = '!=',
  GreaterThan = '>',
  LessThan = '<',
  GreaterThanOrEqual = '>=',
  LessThanOrEqual = '<=',
  Contains = '@=',
}

export const possibleOperators = [
  FilterOperator.Equal,
  FilterOperator.NotEqual,
  FilterOperator.GreaterThan,
  FilterOperator.LessThan,
  FilterOperator.GreaterThanOrEqual,
  FilterOperator.LessThanOrEqual,
  FilterOperator.Contains,
].sort((a, b) => b.length - a.length); // Sorts operators by length. Higher length first
