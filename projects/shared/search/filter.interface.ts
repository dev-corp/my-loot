import { FilterOperator } from './filter-operator';


export interface IFilter {
  key: string;
  operator: FilterOperator | string;
  value: string | number;
}
