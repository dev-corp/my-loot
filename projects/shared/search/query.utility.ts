import { FilterOperator, possibleOperators } from './filter-operator';
import { ISort } from './sort.interface';
import { IFilter } from './filter.interface';
import { IQuery } from './query.interface';


export class QueryUtility {
  public static pageParamKey: string = '_page';
  public static limitParamKey: string = '_limit';
  public static sortParamKey: string = '_sort';

  constructor() {
  }

  public static queryToQueryParams(query: IQuery): { [header: string]: string } {
    const params = {};

    if (!query) {
      return params;
    }

    // Pagination
    if (query.page) {
      params[QueryUtility.pageParamKey] = query.page;
    }
    if (query.limit) {
      params[QueryUtility.limitParamKey] = query.limit;
    }

    // Sort
    if (query.sortBy && query.sortBy.length > 0) {
      let sortValue = '';
      query.sortBy.forEach((sort, index, array) => {
        sort.order === 'ASC'
          ? sortValue += sort.key
          : sortValue += `-${sort.key}`;

        if (index < array.length - 1) {
          sortValue += ',';
        }
      });
      params[QueryUtility.sortParamKey] = sortValue;
    }

    if (query.filterBy && query.filterBy.length > 0) {
      // Filter
      query.filterBy.forEach((filter) => {
        if (!params[filter.key]) {
          // Allows arrays to be passed, like date (range)
          params[filter.key] = [];
        }
        params[filter.key].push(`${filter.operator}${filter.value}`);
      });
    }
    return params;
  }

  public static convertQueryOperatorToDbOperator(operator: FilterOperator | string, value: string | number | null): string {
    if (operator === FilterOperator.Equal && value === null) {
      return `IS`;
    } else if (operator === FilterOperator.NotEqual && value === null) {
      return `IS NOT`;
    } else if (operator === FilterOperator.Equal) {
      return `=`;
    } else if (operator === FilterOperator.NotEqual) {
      return `<>`;
    } else if (operator === FilterOperator.Contains) {
      return `LIKE`;
    } else {
      return `${operator}`;
    }
  }

  public static parseValueByOperator(operator: FilterOperator | string, value: string | number): string | number {
    switch (operator) {
      case FilterOperator.Contains:
        return `%${value}%`;
      default:
        return value;
    }
  }

  public static queryParamsToQuery(queryParams: { [key: string]: string }, defaultLimit: number = 100): IQuery {
    return {
      page: +queryParams._page || 0,
      limit: +queryParams._limit || defaultLimit,
      sortBy: this.parseSort(queryParams._sort),
      filterBy: this.parseFilter(queryParams),
    };
  }

  private static parseSort(sort: string): ISort[] {
    if (!sort || sort.length === 0) {
      return [];
    }

    const sorts = sort.split(',');

    const parsedSorts: ISort[] = [];

    sorts.forEach((s) => {
      if (s.startsWith('-')) {
        parsedSorts.push({ key: s.substring(1), order: 'DESC' });
      } else {
        parsedSorts.push({ key: s, order: 'ASC' });
      }
    });

    return parsedSorts;
  }

  private static parseFilter(queryParams: { [key: string]: string }): IFilter[] {
    const filterKeys = Object.keys(queryParams).filter((key) => !key.startsWith('_'));

    const filters: IFilter[] = [];

    filterKeys.forEach((key) => {
      const filterStr = queryParams[key];
      if (Array.isArray(filterStr)) {
        filterStr.forEach((str) => filters.push(this.parseFilterStr(key, str)));
      } else {
        const match = filterStr.match(/,[=:!<>@]+/g);
        if (match && match.length > 0) {
          // Split array elements in the string
          // Case then array query params appear as string with coma separate elements
          const arr = [];
          let start = 0;
          let count = 0;
          do {
            let end = filterStr.indexOf(match[count]);
            if (end === -1) {
              end = filterStr.length - 1;
            }
            arr.push(filterStr.substr(start, end));
            start = end + 1; // plus ,
            count++;
          } while (count < match.length + 1);

          arr.forEach((str) => filters.push(this.parseFilterStr(key, str)));
        } else {
          filters.push(this.parseFilterStr(key, filterStr));
        }
      }
    });

    return filters;
  }

  private static parseFilterStr(key: string, filterStr: string): IFilter {
    let operator = possibleOperators.find((op) => filterStr.trim().startsWith(op));
    let value: any;
    // Default operator
    // In case no operator is provided
    if (operator) {
      value = filterStr.substring(operator.length);
    } else {
      operator = FilterOperator.Equal;
      value = filterStr;
    }

    // Parse value to something more meaningful
    if ((value as string).toLowerCase() === 'true') {
      // Parse boolean
      value = true;
    } else if ((value as string).toLowerCase() === 'false') {
      // Parse boolean
      value = false;
    } else if ((value as string).toLowerCase() === 'null') {
      // Parse null
      value = null;
      // } else if (value.toLowerCase() === 'undefined') {
      //   value = undefined;
    } else if ((value as string).match(/^\d*\.?\d*$/)) {
      value = parseFloat(value as string);
    }

    return {
      key,
      operator,
      // Parse to number if possible
      value,
    };
  }
}
