export interface IAccount {
    id?: string;
    name: string;
    currentBalance?: number;
}
