export interface ICategory {
  id?: string;
  name?: string;
  parentCategory?: ICategory;
  childrenCategories?: ICategory[];
  isSystem?: boolean;
  isParent?: boolean;
  isChild?: boolean;
}
