import { IPayee } from './payee.interface';
import { IAccount } from './account.interface';
import { ICategory } from './category.interface';


export interface ITransaction {
  id?: string;
  date?: Date;
  account?: IAccount; // TODO: this should be required
  payee?: IPayee;
  category?: ICategory;
  memo?: string;
  amount?: number;
  transferTransaction?: ITransaction;
}
