export interface IPayee {
  id?: string;
  name?: string;
  isAccount?: boolean;
  isSystem?: boolean;
}
