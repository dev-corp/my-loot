import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialCreate1589400916863 implements MigrationInterface {
    name = 'InitialCreate1589400916863'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "payee" ("id" varchar PRIMARY KEY NOT NULL, "name" varchar NOT NULL, "isAccount" boolean NOT NULL DEFAULT (0), "isSystem" boolean NOT NULL DEFAULT (0), CONSTRAINT "UQ_09f0437c922d2ab625f650a6b66" UNIQUE ("name"))`, undefined);
        await queryRunner.query(`CREATE TABLE "category" ("id" varchar PRIMARY KEY NOT NULL, "name" varchar NOT NULL, "isSystem" boolean NOT NULL DEFAULT (0), "parentId" varchar, CONSTRAINT "UQ_23c05c292c439d77b0de816b500" UNIQUE ("name"))`, undefined);
        await queryRunner.query(`CREATE TABLE "transaction" ("id" varchar PRIMARY KEY NOT NULL, "date" date, "memo" varchar, "amount" float, "accountId" varchar, "payeeId" varchar, "categoryId" varchar, "transferTransactionId" varchar)`, undefined);
        await queryRunner.query(`CREATE TABLE "account" ("id" varchar PRIMARY KEY NOT NULL, "name" varchar NOT NULL, CONSTRAINT "UQ_414d4052f22837655ff312168cb" UNIQUE ("name"))`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_category" ("id" varchar PRIMARY KEY NOT NULL, "name" varchar NOT NULL, "isSystem" boolean NOT NULL DEFAULT (0), "parentId" varchar, CONSTRAINT "UQ_23c05c292c439d77b0de816b500" UNIQUE ("name"), CONSTRAINT "FK_d5456fd7e4c4866fec8ada1fa10" FOREIGN KEY ("parentId") REFERENCES "category" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_category"("id", "name", "isSystem", "parentId") SELECT "id", "name", "isSystem", parentCategoryId FROM "category"`, undefined);
        await queryRunner.query(`DROP TABLE "category"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_category" RENAME TO "category"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_transaction" ("id" varchar PRIMARY KEY NOT NULL, "date" date, "memo" varchar, "amount" float, "accountId" varchar, "payeeId" varchar, "categoryId" varchar, "transferTransactionId" varchar, CONSTRAINT "FK_3d6e89b14baa44a71870450d14d" FOREIGN KEY ("accountId") REFERENCES "account" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_70815d35b9b0fe366cc9014cb9e" FOREIGN KEY ("payeeId") REFERENCES "payee" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_d3951864751c5812e70d033978d" FOREIGN KEY ("categoryId") REFERENCES "category" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_cf8700608192ffe84c16df4cc29" FOREIGN KEY ("transferTransactionId") REFERENCES "transaction" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_transaction"("id", "date", "memo", "amount", "accountId", "payeeId", "categoryId", "transferTransactionId") SELECT "id", "date", "memo", "amount", "accountId", "payeeId", "categoryId", "transferTransactionId" FROM "transaction"`, undefined);
        await queryRunner.query(`DROP TABLE "transaction"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_transaction" RENAME TO "transaction"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" RENAME TO "temporary_transaction"`, undefined);
        await queryRunner.query(`CREATE TABLE "transaction" ("id" varchar PRIMARY KEY NOT NULL, "date" date, "memo" varchar, "amount" float, "accountId" varchar, "payeeId" varchar, "categoryId" varchar, "transferTransactionId" varchar)`, undefined);
        await queryRunner.query(`INSERT INTO "transaction"("id", "date", "memo", "amount", "accountId", "payeeId", "categoryId", "transferTransactionId") SELECT "id", "date", "memo", "amount", "accountId", "payeeId", "categoryId", "transferTransactionId" FROM "temporary_transaction"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_transaction"`, undefined);
        await queryRunner.query(`ALTER TABLE "category" RENAME TO "temporary_category"`, undefined);
        await queryRunner.query(`CREATE TABLE "category" ("id" varchar PRIMARY KEY NOT NULL, "name" varchar NOT NULL, "isSystem" boolean NOT NULL DEFAULT (0), "parentId" varchar, CONSTRAINT "UQ_23c05c292c439d77b0de816b500" UNIQUE ("name"))`, undefined);
        await queryRunner.query(`INSERT INTO "category"("id", "name", "isSystem", parentCategoryId) SELECT "id", "name", "isSystem", "parentId" FROM "temporary_category"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_category"`, undefined);
        await queryRunner.query(`DROP TABLE "account"`, undefined);
        await queryRunner.query(`DROP TABLE "transaction"`, undefined);
        await queryRunner.query(`DROP TABLE "category"`, undefined);
        await queryRunner.query(`DROP TABLE "payee"`, undefined);
    }

}
