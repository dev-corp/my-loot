import { ICategory } from 'shared/interfaces';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TransactionEntity } from './transaction.entity';


@Entity('category')
export class CategoryEntity implements ICategory {

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'varchar', unique: true, nullable: false })
  public name: string;

  @ManyToOne((type) => CategoryEntity, (category) => category.childrenCategories, { nullable: true })
  public parentCategory?: CategoryEntity;

  @OneToMany((type) => CategoryEntity, (category) => category.parentCategory, { nullable: true })
  public childrenCategories?: CategoryEntity[];

  @OneToMany((type) => TransactionEntity, (transaction) => transaction.category, { nullable: true })
  public transactions?: TransactionEntity[];

  @Column({ type: 'boolean', nullable: false, default: false })
  public isSystem?: boolean;

  public get isParent(): boolean {
    return this.parentCategory === null
      || Array.isArray(this.childrenCategories)
      || this.parentCategoryId === null;
  }

  public get isChild(): boolean {
    return !this.isParent;
  }

  public parentCategoryId?: string;

  constructor(categoryObj?: ICategory) {
    if (categoryObj) {
      this.id = categoryObj.id;
      this.name = categoryObj.name;
      if (categoryObj.parentCategory !== undefined) {
        this.parentCategoryId = categoryObj.parentCategory ? categoryObj.parentCategory.id : null;
        this.parentCategory = categoryObj.parentCategory ? new CategoryEntity(categoryObj.parentCategory) : null;
      }
      if (categoryObj.childrenCategories !== undefined) {
        this.childrenCategories = categoryObj.childrenCategories
          ? categoryObj.childrenCategories.map((child) => child instanceof CategoryEntity ? child : new CategoryEntity(child))
          : [];
      }
      this.isSystem = categoryObj.isSystem;
    }
  }
}
