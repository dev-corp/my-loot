import { IAccount } from 'shared/interfaces';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TransactionEntity } from './transaction.entity';


@Entity('account')
export class AccountEntity implements IAccount {

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'varchar', unique: true, nullable: false })
  public name: string;

  @OneToMany((type) => TransactionEntity, (transaction) => transaction.account, { nullable: true })
  public transactions?: TransactionEntity[];

  constructor(accountObj?: IAccount) {
    if (accountObj) {
      this.id = accountObj.id;
      this.name = accountObj.name;
    }
  }
}
