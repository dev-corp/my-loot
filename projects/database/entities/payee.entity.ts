import { IPayee } from 'shared/interfaces';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TransactionEntity } from './transaction.entity';


@Entity('payee')
export class PayeeEntity implements IPayee {

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'varchar', unique: true, nullable: false })
  public name: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isAccount?: boolean = false;

  @OneToMany((type) => TransactionEntity, (transaction) => transaction.payee, { nullable: true })
  public transactions?: TransactionEntity[];

  @Column({ type: 'boolean', nullable: false, default: false })
  public isSystem?: boolean;

  constructor(payeeObj?: IPayee) {
    if (payeeObj) {
      this.id = payeeObj.id;
      this.name = payeeObj.name;
      this.isAccount = payeeObj.isAccount;
      this.isSystem = payeeObj.isSystem;
    }
  }
}
