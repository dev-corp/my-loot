import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ITransaction } from 'shared/interfaces';
import { AccountEntity } from './account.entity';
import { PayeeEntity } from './payee.entity';
import { CategoryEntity } from './category.entity';


@Entity('transaction')
export class TransactionEntity implements ITransaction {

  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @Column({ type: 'date', nullable: true })
  public date?: Date;

  @ManyToOne((type) => AccountEntity, (account) => account.transactions, { nullable: true })
  public account?: AccountEntity;

  @ManyToOne((type) => PayeeEntity, (payee) => payee.transactions, { nullable: true })
  public payee?: PayeeEntity;

  @ManyToOne(
    (type) => CategoryEntity,
    (category) => category.transactions,
    { nullable: true },
  )
  public category?: CategoryEntity;

  @Column({ type: 'varchar', nullable: true })
  public memo?: string;

  @Column({ type: 'float', nullable: true })
  public amount?: number;

  @ManyToOne(
    (type) => TransactionEntity,
    (transaction) => transaction.transferTransaction,
    { nullable: true },
  )
  public transferTransaction?: TransactionEntity;

  constructor(transactionObj?: ITransaction) {
    if (transactionObj) {
      this.id = transactionObj.id;
      this.date = transactionObj.date;
      this.memo = transactionObj.memo;
      this.amount = +transactionObj.amount.toFixed(2);
      if (transactionObj.account !== undefined) {
        this.account = transactionObj.account ? new AccountEntity(transactionObj.account) : null;
      }
      if (transactionObj.payee !== undefined) {
        this.payee = transactionObj.payee ? new PayeeEntity(transactionObj.payee) : null;
      }
      if (transactionObj.category !== undefined) {
        this.category = transactionObj.category ? new CategoryEntity(transactionObj.category) : null;
      }
      if (transactionObj.transferTransaction !== undefined) {
        this.transferTransaction = transactionObj.transferTransaction ? new TransactionEntity(transactionObj.transferTransaction) : null;
      }
    }
  }
}
