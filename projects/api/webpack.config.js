'use strict';
const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const exec = require('child_process').exec;


const tsConfigPath = 'projects/api/tsconfig.api.json';
const outputPath = 'dist/web';

module.exports = {
  target: 'node',
  mode: 'development',
  devtool: 'inline-source-map',
  entry: './projects/api/server.ts',
  node: {
    __dirname: false
  },
  module: {
    rules: [
      {
        // TypeScript
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: tsConfigPath
        }
      },
    ]
  },
  resolve: {
    plugins: [new TsconfigPathsPlugin({ configFile: tsConfigPath })],
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'api.bundle.js',
    path: path.resolve(__dirname, '../../', outputPath, 'api'),
  },
  externals: [nodeExternals()],
  plugins: [
    {
      apply: (compiler) => {
        compiler.hooks.afterEmit.tap('Build client', (compilation) => {
          // Build migrations and entities
          exec(`tsc --project projects/database/tsconfig.json --outDir ${outputPath} && rimraf ${outputPath}/shared`, (err, stdout, stderr) => {
            if (stdout) process.stdout.write(stdout);
            if (stderr) process.stderr.write(stderr);
          });
        });
      }
    }
  ]
};
