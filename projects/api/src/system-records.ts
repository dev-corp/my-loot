import { SystemCategoryId, SystemPayeeId } from 'shared/enums';
import { CategoryEntity, PayeeEntity } from 'database/entities';


const systemCategory = new CategoryEntity({
  id: SystemCategoryId.SystemMasterCategory,
  name: 'System',
  isSystem: true,
});

const systemIncome = new CategoryEntity({
  id: SystemCategoryId.SystemIncome,
  name: 'Income',
  parentCategory: systemCategory,
  isSystem: true,
});

export const SYSTEM_CATEGORIES: CategoryEntity[] = [
  systemCategory,
  systemIncome,
];

const systemPayeeStartingBalance = new PayeeEntity({
  id: SystemPayeeId.SystemStartingBalance,
  name: 'Starting Balance',
  isSystem: true,
});

const systemPayeeAdjustBalance = new PayeeEntity({
  id: SystemPayeeId.SystemAdjustBalance,
  name: 'Adjust Balance',
  isSystem: true,
});
export const SYSTEM_PAYEES: PayeeEntity[] = [
  systemPayeeStartingBalance,
  systemPayeeAdjustBalance,
];

