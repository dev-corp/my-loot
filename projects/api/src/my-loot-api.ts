import 'reflect-metadata';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as http from 'http';
import * as cors from 'cors';
import { Express } from 'express';
import { Connection, ConnectionOptions, createConnection, getConnection } from 'typeorm';
import { AccountRepository, CategoryRepository, PayeeRepository, TransactionRepository } from 'src/repositories';
import { AccountController, CategoryController, PayeeController, TransactionController } from 'src/controllers';
import { apiErrorHandlerFactory, DbQueryUtility } from 'src/utils';
import { CategoryEntity, PayeeEntity } from 'database/entities';
import { SYSTEM_CATEGORIES, SYSTEM_PAYEES } from 'src/system-records';


export interface IMyLootApiOptions {
  port?: number;
}

const defaultOptions: IMyLootApiOptions = {
  port: 9200,
};


export class MyLootApi {

  public readonly app: Express;
  public readonly httpServer: http.Server;

  private readonly _options: IMyLootApiOptions;

  // Dependencies
  private readonly dbQueryUtility: DbQueryUtility;

  private readonly accountRepository: AccountRepository;
  private readonly categoryRepository: CategoryRepository;
  private readonly payeeRepository: PayeeRepository;
  private readonly transactionRepository: TransactionRepository;

  private readonly accountController: AccountController;
  private readonly categoryController: CategoryController;
  private readonly payeeController: PayeeController;
  private readonly transactionController: TransactionController;
  private readonly connectionPromise: Promise<Connection>;

  constructor(dbConnectionOptions: ConnectionOptions, options: IMyLootApiOptions = {}) {
    this._options = { ...defaultOptions, ...options };
    this.app = express();
    this.httpServer = http.createServer(this.app);

    this.connectionPromise = createConnection(dbConnectionOptions);
    this.connectionPromise.then((connection) => { console.log(`Established connection to database`); })
      .catch((error) => { console.error(error); });

    // Dependency Injection BEGIN
    this.dbQueryUtility = new DbQueryUtility();
    this.payeeRepository = new PayeeRepository(getConnection());
    this.categoryRepository = new CategoryRepository(getConnection());
    this.accountRepository = new AccountRepository(getConnection(), this.payeeRepository, null /* circular dependency resolved below */);
    this.transactionRepository = new TransactionRepository(getConnection(), this.dbQueryUtility, this.accountRepository, this.payeeRepository);
    // @ts-ignore Fix circular dependency
    this.accountRepository.transactionRepository = this.transactionRepository;

    this.accountController = new AccountController(this.accountRepository);
    this.categoryController = new CategoryController(this.categoryRepository);
    this.payeeController = new PayeeController(this.payeeRepository);
    this.transactionController = new TransactionController(this.transactionRepository, this.dbQueryUtility);
    // Dependency Injection END

    this.insertSystemRecords();
    this.registerMiddleware();
    this.registerRoutes();

    this.httpServer.listen(this._options.port, () => {
      console.log(`My Loot API listening on http://localhost:${this._options.port}`);
    });

    // Error handlers
    this.app.use(apiErrorHandlerFactory(this.app.get('env')));
  }

  private registerMiddleware(): void {
    this.app.use(cors({
      origin: ['http://localhost:4200'],
    }));

    this.app.use(bodyParser.json());
  }

  private registerRoutes(): void {
    this.app.use('/accounts', this.accountController.routes);
    this.app.use('/categories', this.categoryController.routes);
    this.app.use('/payees', this.payeeController.routes);
    this.app.use('/transactions', this.transactionController.routes);

    this.app.get('/', (req, res, next) => {
      res.send('Welcome to My Loot API');
    });
  }

  private insertSystemRecords(): void {
    this.connectionPromise.then((connection) => {
      connection.manager.getRepository(CategoryEntity).save(SYSTEM_CATEGORIES);
      connection.manager.getRepository(PayeeEntity).save(SYSTEM_PAYEES);
    });
  }
}
