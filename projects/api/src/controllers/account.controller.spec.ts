import { NextFunction, Response } from 'express-serve-static-core';
import { MockAccountRepository } from 'src/mocks';
import { AccountController } from './account.controller';


describe('AccountController', () => {

  let mockAccountRepository: MockAccountRepository;
  let accountController: AccountController;
  let res: Response;
  let next: NextFunction;

  beforeEach(() => {
    mockAccountRepository = new MockAccountRepository();
    accountController = new AccountController(mockAccountRepository);
    // @ts-ignore
    res = { statusCode: null, send: jasmine.createSpy('send') };
    next = jasmine.createSpy('next');
  });

  it('should be created', () => {
    expect(accountController).toBeDefined();
  });
});
