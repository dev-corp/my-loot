import { AccountRepository } from 'src/repositories';
import { Router } from 'express';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { Account } from 'shared/models';
import { IAccount } from 'shared/interfaces';
import { constants } from 'http2';
import { CustomError } from 'ts-custom-error';


export class AccountController {

  public readonly routes: Router;

  constructor(private accountRepository: AccountRepository) {
    this.routes = Router();
    this.routes.get('/', (req, res, next) => this.getAllAccountsAsync(req, res, next));
    this.routes.post('/', (req, res, next) => this.createAccountAsync(req, res, next));
    this.routes.put('/:accountId', (req, res, next) => this.updateAccountAsync(req, res, next));
  }

  private async getAllAccountsAsync(req: Request, res: Response<Account[]>, next: NextFunction): Promise<Response> {
    try {
      const loadCurrentBalance = req.query.loadCurrentBalance;
      const response = loadCurrentBalance
        ? await this.accountRepository.getAllAccountsWithBalanceAsync()
        : await this.accountRepository.getAllAccountsAsync();
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }

  private async createAccountAsync(req: Request<any, any, IAccount & { currentBalance: number }>,
                                   res: Response<Account>,
                                   next: NextFunction): Promise<Response> {
    try {
      // TODO: Create account payee for transfers
      const account = new Account(req.body);
      const currentBalance = req.body.currentBalance;
      const response = await this.accountRepository.createAccountAsync(account, currentBalance);
      res.statusCode = constants.HTTP_STATUS_CREATED;
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }

  private async updateAccountAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const accountId = req.params.accountId;
      const account = new Account(req.body);

      if (accountId !== account.id) {
        throw new CustomError('accountId does not match account object');
      }

      const balanceAdjust = req.body.currentBalance;
      const response = await this.accountRepository.updateAccountAsync(account, balanceAdjust);
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }
}
