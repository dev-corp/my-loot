import { PayeeController } from './payee.controller';
import { Payee } from 'shared/models';
import { IPayee } from 'shared/interfaces';
import { Response, NextFunction } from 'express-serve-static-core';
import { CustomError } from 'ts-custom-error';
import { MockPayeeRepository } from 'src/mocks';


describe('PayeeController', () => {

  let mockPayeeRepository: MockPayeeRepository;
  let payeeController: PayeeController;
  let res: Response;
  let next: NextFunction;

  beforeEach(() => {
    mockPayeeRepository = new MockPayeeRepository();
    payeeController = new PayeeController(mockPayeeRepository);
    // @ts-ignore
    res = { statusCode: null, send: jasmine.createSpy('send') };
    next = jasmine.createSpy('next');
  });

  it('should be created', () => {
    expect(payeeController).toBeDefined();
  });

  describe('getAllPayeesAsync', () => {
    it('should call getAllPayeesAsync on repository', async () => {
      // Arrange
      mockPayeeRepository.getAllPayeesAsync.and.returnValue(Promise.resolve([]));

      // Act
      // @ts-ignore
      await payeeController.getAllPayeesAsync({}, res, next);

      // Assert
      expect(res.statusCode).toEqual(200);
      expect(res.send).toHaveBeenCalled();
      expect(next).not.toHaveBeenCalled();
    });

    it('should call next on error', async () => {
      // Arrange
      mockPayeeRepository.getAllPayeesAsync.and.returnValue(Promise.reject());

      // Act
      // @ts-ignore
      await payeeController.getAllPayeesAsync({}, res, next);

      // Assert
      expect(res.send).not.toHaveBeenCalled();
      expect(next).toHaveBeenCalled();
    });
  });

  describe('createPayeeAsync', () => {
    it('should call createPayeeAsync on repository', async () => {
      // Arrange
      const payee: IPayee = { name: 'Test Payee' };
      const expected = new Payee({ ...payee, ...{ id: 'testId' } });
      const expectedArgument = new Payee(payee);
      delete expectedArgument.isAccount;

      mockPayeeRepository.createPayeeAsync.and.returnValue(Promise.resolve(expected));

      // Act
      // @ts-ignore
      await payeeController.createPayeeAsync({ body: payee }, res, next);

      // Assert
      expect(mockPayeeRepository).toBeTruthy();
      expect(mockPayeeRepository.createPayeeAsync).toHaveBeenCalled();
      expect(mockPayeeRepository.createPayeeAsync).toHaveBeenCalledWith(expectedArgument);
      expect(res.statusCode).toEqual(201);
      expect(res.send).toHaveBeenCalled();
      expect(next).not.toHaveBeenCalled();
    });

    it('should remove isAccount from body', async () => {
      // Arrange
      const payee: IPayee = { name: 'Test Payee', isAccount: true };

      // Act
      // @ts-ignore
      await payeeController.createPayeeAsync({ body: payee }, res, next);

      // Assert
      expect(mockPayeeRepository.createPayeeAsync).toHaveBeenCalled();
      expect(mockPayeeRepository.createPayeeAsync)
        .not.toHaveBeenCalledWith(jasmine.objectContaining({ isAccount: jasmine.any(Boolean) }));
      expect(res.statusCode).toEqual(201);
      expect(res.send).toHaveBeenCalled();
      expect(next).not.toHaveBeenCalled();
    });

    it('should call next on error', async () => {
      // Arrange
      const payee: IPayee = { name: 'Test Payee' };

      mockPayeeRepository.createPayeeAsync.and.returnValue(Promise.reject());

      // Act
      // @ts-ignore
      await payeeController.createPayeeAsync({ body: payee }, res, next);

      // Assert
      expect(mockPayeeRepository.createPayeeAsync).toHaveBeenCalled();
      expect(res.send).not.toHaveBeenCalled();
      expect(next).toHaveBeenCalled();
    });
  });

  describe('updatePayeeAsync', () => {
    it('should call updatePayeeAsync on repository', async () => {
      // Arrange
      const id = 'testId';
      const req = {
        params: { payeeId: id },
        body: { id, name: 'Payee Name' },
      };
      mockPayeeRepository.updatePayeeAsync.and.returnValue(Promise.resolve(new Payee(req.body)));

      // Act
      // @ts-ignore
      await payeeController.updatePayeeAsync(req, res, next);

      // Assert
      expect(res.statusCode).toEqual(200);
      expect(res.send).toHaveBeenCalled();
      expect(next).not.toHaveBeenCalled();
    });

    it('should remove isAccount from body', async () => {
      // Arrange
      const id = 'testId';
      const req = {
        params: { payeeId: id },
        body: { id, name: 'Payee Name' },
      };

      // Act
      // @ts-ignore
      await payeeController.updatePayeeAsync(req, res, next);

      // Assert
      expect(mockPayeeRepository.updatePayeeAsync).toHaveBeenCalled();
      expect(mockPayeeRepository.updatePayeeAsync)
        .not.toHaveBeenCalledWith(jasmine.objectContaining({ isAccount: jasmine.any(Boolean) }));
      expect(res.statusCode).toEqual(200);
      expect(res.send).toHaveBeenCalled();
      expect(next).not.toHaveBeenCalled();
    });

    it('should call next on error', async () => {
      // Arrange
      const id = 'testId';
      const req = {
        params: { payeeId: id },
        body: { id, name: 'Payee Name' },
      };
      mockPayeeRepository.updatePayeeAsync.and.returnValue(Promise.reject());

      // Act
      // @ts-ignore
      await payeeController.updatePayeeAsync(req, res, next);

      // Assert
      expect(mockPayeeRepository.updatePayeeAsync).toHaveBeenCalled();
      expect(res.send).not.toHaveBeenCalled();
      expect(next).toHaveBeenCalled();
    });

    it('should throw error when payeeId does not match with body.payee.id', async () => {
      // Arrange
      const req = {
        params: { payeeId: 'idCorrect' },
        body: { id: ' wrongId', name: 'Payee Name' },
      };

      // Act
      // @ts-ignore
      await payeeController.updatePayeeAsync(req, res, next);

      // Assert
      expect(mockPayeeRepository.updatePayeeAsync).not.toHaveBeenCalled();
      expect(res.send).not.toHaveBeenCalled();
      expect(next).toHaveBeenCalled();
      // @ts-ignore
      expect(next).toHaveBeenCalledWith(new CustomError('payeeId does not match account object'));
    });
  });

});
