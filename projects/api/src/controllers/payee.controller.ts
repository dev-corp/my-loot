import { PayeeRepository } from 'src/repositories';
import { Router } from 'express';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { Payee } from 'shared/models';
import { IPayee } from 'shared/interfaces';
import { constants } from 'http2';
import { CustomError } from 'ts-custom-error';


export class PayeeController {

  public readonly routes: Router;

  constructor(private payeeRepository: PayeeRepository) {
    this.routes = Router();
    this.routes.get('/', (req, res, next) => this.getAllPayeesAsync(req, res, next));
    this.routes.post('/', (req, res, next) => this.createPayeeAsync(req, res, next));
    this.routes.put('/:payeeId', (req, res, next) => this.updatePayeeAsync(req, res, next));
  }

  public async getAllPayeesAsync(req: Request, res: Response<Payee[]>, next: NextFunction): Promise<Response> {
    try {
      const response = await this.payeeRepository.getAllPayeesAsync();
      res.statusCode = constants.HTTP_STATUS_OK;
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }

  public async createPayeeAsync(req: Request<any, any, IPayee>,
                                res: Response<Payee>,
                                next: NextFunction): Promise<Response> {
    try {
      const payee = new Payee(req.body);

      // Sanitize isAccount
      delete payee.isAccount;

      const response = await this.payeeRepository.createPayeeAsync(payee);
      res.statusCode = constants.HTTP_STATUS_CREATED;
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }

  public async updatePayeeAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const payeeId = req.params.payeeId;
      const payee = new Payee(req.body);

      if (payeeId !== payee.id) {
        throw new CustomError('payeeId does not match account object');
      }

      // Sanitize isAccount
      delete payee.isAccount;

      const response = await this.payeeRepository.updatePayeeAsync(payee);
      res.statusCode = constants.HTTP_STATUS_OK;
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }
}
