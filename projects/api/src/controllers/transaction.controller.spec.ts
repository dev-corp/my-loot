import { NextFunction, Response } from 'express-serve-static-core';
import { MockDbQueryUtility, MockTransactionRepository } from 'src/mocks';
import { TransactionController } from './transaction.controller';


describe('TransactionController', () => {

  let mockTransactionRepository: MockTransactionRepository;
  let mockDbQueryUtility: MockDbQueryUtility;
  let transactionController: TransactionController;
  let res: Response;
  let next: NextFunction;

  beforeEach(() => {
    mockTransactionRepository = new MockTransactionRepository();
    mockDbQueryUtility = new MockDbQueryUtility();
    transactionController = new TransactionController(mockTransactionRepository, mockDbQueryUtility);
    // @ts-ignore
    res = { statusCode: null, send: jasmine.createSpy('send') };
    next = jasmine.createSpy('next');
  });

  it('should be created', () => {
    expect(transactionController).toBeDefined();
  });
});
