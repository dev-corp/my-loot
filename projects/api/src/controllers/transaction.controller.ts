import { Router } from 'express';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { constants } from 'http2';
import { Transaction } from 'shared/models';
import { TransactionRepository } from 'src/repositories';
import { DbQueryUtility } from 'src/utils';
import { CustomError } from 'ts-custom-error';


// TODO: Add soft delete
// TODO: Add option for creating transfer

export class TransactionController {

  public readonly routes: Router;

  constructor(private transactionRepository: TransactionRepository,
              private dbQueryUtility: DbQueryUtility) {
    this.routes = Router();
    this.routes.get('/', (req, res, next) => this.queryTransactionsAsync(req, res, next));
    this.routes.post('/', (req, res, next) => this.createTransactionAsync(req, res, next));
    this.routes.put('/', (req, res, next) => this.updateMultipleTransactionsAsync(req, res, next));
    this.routes.put('/:transactionId', (req, res, next) => this.updateTransactionAsync(req, res, next));
    this.routes.delete('/', (req, res, next) => this.deleteMultipleTransactionsAsync(req, res, next));
    this.routes.delete('/:transactionId', (req, res, next) => this.deleteTransactionAsync(req, res, next));
  }

  public async queryTransactionsAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const query = this.dbQueryUtility.parseQueryParamsToQuery(req.query);
      const result = await this.transactionRepository.queryTransactionsAsync(query);
      return res.send({ transactions: result.transactions, query, total: result.total });
    } catch (e) {
      next(e);
    }
  }

  public async createTransactionAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const newTransaction = new Transaction(req.body);
      const transaction = await this.transactionRepository.createTransactionAsync(newTransaction);
      return res.status(constants.HTTP_STATUS_CREATED).send(transaction);
    } catch (e) {
      next(e);
    }
  }

  public async updateTransactionAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const transactionId = req.params.transactionId;
      const transactionToUpdate = new Transaction(req.body);

      if (transactionId !== transactionToUpdate.id) {
        throw new CustomError('transactionId does not match transaction object');
      }

      const updatedTransaction = await this.transactionRepository.updateTransactionAsync(transactionToUpdate);
      return res.status(constants.HTTP_STATUS_OK).send(updatedTransaction);
    } catch (e) {
      next(e);
    }
  }


  public async updateMultipleTransactionsAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      if (Array.isArray(req.body)) {
        const transactions = req.body.map((transaction) => new Transaction(transaction));
        const updatedTransactions = [];
        await transactions.reduce((prev, transaction) => {
          return prev.then((updatedTransaction) => {
            if (updatedTransaction) {
              updatedTransactions.push(updatedTransaction);
            }
            return this.transactionRepository.updateTransactionAsync(transaction);
          });
        }, Promise.resolve(null));
        return res.status(constants.HTTP_STATUS_OK).send(updatedTransactions);
      } else {
        res.status(constants.HTTP_STATUS_BAD_REQUEST).send();
      }
    } catch (e) {
      next(e);
    }
  }

  public async deleteTransactionAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const transactionId = req.params.transactionId;
      await this.transactionRepository.deleteTransactionAsync(transactionId);
      return res.status(constants.HTTP_STATUS_NO_CONTENT).send();
    } catch (e) {
      next(e);
    }
  }

  public async deleteMultipleTransactionsAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      if (Array.isArray(req.body)) {
        const transactions = req.body.map((transaction) => new Transaction(transaction));
        await transactions.reduce((prev, transaction) => {
          return prev.then(() => {
            return this.transactionRepository.deleteTransactionAsync(transaction.id);
          });
        }, Promise.resolve());
      }
      return res.status(constants.HTTP_STATUS_NO_CONTENT).send();
    } catch (e) {
      next(e);
    }
  }
}
