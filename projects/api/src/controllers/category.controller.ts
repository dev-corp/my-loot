import { CategoryRepository } from 'src/repositories';
import { Router } from 'express';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { Category } from 'shared/models';
import { ICategory } from 'shared/interfaces';
import { constants } from 'http2';
import { CustomError } from 'ts-custom-error';


export class CategoryController {

  public readonly routes: Router;

  constructor(private categoryRepository: CategoryRepository) {
    this.routes = Router();
    this.routes.get('/', (req, res, next) => this.getAllCategoriesAsync(req, res, next));
    this.routes.post('/', (req, res, next) => this.createCategoryAsync(req, res, next));
    this.routes.put('/:categoryId', (req, res, next) => this.updateCategoryAsync(req, res, next));
  }

  private async getAllCategoriesAsync(req: Request, res: Response<Category[]>, next: NextFunction): Promise<Response> {
    try {
      const response = await this.categoryRepository.getAllCategoriesAsync();
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }

  private async createCategoryAsync(req: Request<any, any, ICategory>,
                                    res: Response<Category>,
                                    next: NextFunction): Promise<Response> {
    try {
      const category = new Category(req.body);
      const response = await this.categoryRepository.createCategoryAsync(category);
      res.statusCode = constants.HTTP_STATUS_CREATED;
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }

  private async updateCategoryAsync(req: Request, res: Response, next: NextFunction): Promise<Response> {
    try {
      const categoryId = req.params.categoryId;
      const category = new Category(req.body);

      if (categoryId !== category.id) {
        throw new CustomError('categoryId does not match account object');
      }
      const response = await this.categoryRepository.updateCategoryAsync(category);
      return res.send(response);
    } catch (e) {
      next(e);
    }
  }
}
