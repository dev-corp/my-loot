import { NextFunction, Response } from 'express-serve-static-core';
import { MockCategoryRepository } from 'src/mocks';
import { CategoryController } from './category.controller';


describe('CategoryController', () => {

  let mockCategoryRepository: MockCategoryRepository;
  let categoryController: CategoryController;
  let res: Response;
  let next: NextFunction;

  beforeEach(() => {
    mockCategoryRepository = new MockCategoryRepository();
    categoryController = new CategoryController(mockCategoryRepository);
    // @ts-ignore
    res = { statusCode: null, send: jasmine.createSpy('send') };
    next = jasmine.createSpy('next');
  });

  it('should be created', () => {
    expect(categoryController).toBeDefined();
  });
});
