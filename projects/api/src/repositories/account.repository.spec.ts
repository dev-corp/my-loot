import { MockEntityManager, MockPayeeRepository, MockTransactionRepository } from 'src/mocks';

import { AccountRepository } from './account.repository';


describe('AccountRepository', () => {
  let mockManager: MockEntityManager;
  let mockPayeeRepository: MockPayeeRepository;
  let mockTransactionRepository: MockTransactionRepository;

  let accountRepository: AccountRepository;

  beforeEach(() => {
    mockManager = new MockEntityManager();
    mockPayeeRepository = new MockPayeeRepository();
    mockTransactionRepository = new MockTransactionRepository();
    accountRepository = new AccountRepository({ manager: mockManager } as any, mockPayeeRepository, mockTransactionRepository);
  });

  it('should be created', () => {
    expect(accountRepository).toBeDefined();
  });
});
