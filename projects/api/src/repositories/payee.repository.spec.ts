import { Payee } from 'shared/models';
import { PayeeEntity } from 'database/entities';
import { MockEntityManager } from 'src/mocks';
import { PayeeRepository } from './payee.repository';
import createSpy = jasmine.createSpy;
import { CustomError } from 'ts-custom-error';
import { NotFoundError } from 'src/errors';


describe('PayeeRepository', () => {
  let mockManager: MockEntityManager;
  let payeeRepository: PayeeRepository;

  beforeEach(() => {
    mockManager = new MockEntityManager();
    payeeRepository = new PayeeRepository({ manager: mockManager } as any);
  });

  it('should be created', () => {
    expect(payeeRepository).toBeDefined();
  });

  describe('getAllPayeesAsync', () => {
    it('should get all payees and map them to Payee', async () => {
      // Arrange
      const mockEntities = [
        new PayeeEntity({ id: '1', name: 'Ent 1' }),
        new PayeeEntity({ id: '2', name: 'Ent 2', isAccount: true }),
      ];
      const find = createSpy('find').and.returnValue(Promise.resolve(mockEntities));
      mockManager.getRepository.and.returnValue({ find });
      // Act
      const result = await payeeRepository.getAllPayeesAsync();
      // Assert
      expect(mockManager.getRepository).toHaveBeenCalled();
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(find).toHaveBeenCalled();
      expect(result).toBeDefined();
      expect(result.length).toEqual(mockEntities.length);
      expect(result[0]).toBeInstanceOf(Payee);
      expect(result[1]).toBeInstanceOf(Payee);
      expect(result[0].id).toEqual(mockEntities[0].id);
      expect(result[0].name).toEqual(mockEntities[0].name);
      expect(result[1].isAccount).toEqual(mockEntities[1].isAccount);
    });
  });

  describe('getPayeeByIdAsync', () => {
    it('should get entity by id', async () => {
      // Arrange
      const mockEntity = new PayeeEntity({ id: '1', name: 'Ent 1' });

      const findOne = createSpy('findOne').and.returnValue(Promise.resolve(mockEntity));
      mockManager.getRepository.and.returnValue({ findOne });

      // Act
      const result = await payeeRepository.getPayeeByIdAsync(mockEntity.id);

      // Assert
      expect(mockManager.getRepository).toHaveBeenCalled();
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(findOne).toHaveBeenCalled();
      expect(result).toBeDefined();
      expect(result).toBeInstanceOf(Payee);
      expect(result.id).toEqual(mockEntity.id);
      expect(result.name).toEqual(mockEntity.name);
      expect(result.isAccount).toEqual(mockEntity.isAccount);
    });
  });

  describe('createPayeeAsync', () => {
    it('should create Payee', async () => {
      // Arrange
      const payee = new Payee({ name: 'Ent 1' });
      const mockEntity = new PayeeEntity({ id: '1', name: 'Ent 1', isAccount: false });

      const save = createSpy('save').and.returnValue(Promise.resolve(mockEntity));
      mockManager.getRepository.and.returnValue({ save });

      // Act
      const result = await payeeRepository.createPayeeAsync(payee);

      // Assert
      expect(mockManager.getRepository).toHaveBeenCalled();
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(save).toHaveBeenCalled();
      expect(save).toHaveBeenCalledWith(jasmine.objectContaining({ id: undefined, name: 'Ent 1', isAccount: undefined }));
      expect(result).toBeDefined();
      expect(result).toBeInstanceOf(Payee);
      expect(result.id).toEqual(mockEntity.id);
      expect(result.name).toEqual(mockEntity.name);
      expect(result.isAccount).toEqual(mockEntity.isAccount);
    });
  });

  describe('updatePayeeAsync', () => {
    it('should create Payee', async () => {
      // Arrange
      const payee = new Payee({ id: '1', name: 'Ent 1 Updated' });
      const mockEntity = new PayeeEntity({ id: '1', name: 'Ent 1', isAccount: false });
      const mockEntityUpdated = new PayeeEntity({ id: '1', name: 'Ent 1 Updated', isAccount: false });

      const findOne = createSpy('findOne').and.returnValue(Promise.resolve(mockEntity));
      const save = createSpy('save').and.returnValue(Promise.resolve(mockEntityUpdated));
      mockManager.getRepository.and.returnValue({ save, findOne });

      // Act
      const result = await payeeRepository.updatePayeeAsync(payee);

      // Assert
      expect(mockManager.getRepository).toHaveBeenCalled();
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(findOne).toHaveBeenCalled();
      expect(findOne).toHaveBeenCalledWith(payee.id);
      expect(save).toHaveBeenCalled();
      expect(save).toHaveBeenCalledWith(jasmine.objectContaining({ id: '1', name: 'Ent 1 Updated', isAccount: undefined }));
      expect(result).toBeDefined();
      expect(result).toBeInstanceOf(Payee);
      expect(result.id).toEqual(mockEntityUpdated.id);
      expect(result.name).toEqual(mockEntityUpdated.name);
      expect(result.isAccount).toEqual(mockEntityUpdated.isAccount);
    });

    it('should throw when no entity is found', async () => {
      // Arrange
      const payee = new Payee({ id: '1', name: 'Ent 1 Updated' });

      const findOne = createSpy('findOne').and.returnValue(Promise.resolve(undefined));
      const save = createSpy('save');
      mockManager.getRepository.and.returnValue({ save, findOne });

      // Act
      await payeeRepository.updatePayeeAsync(payee)
        .catch((error: CustomError) => {
          expect(error).toBeDefined();
          expect(error).toBeInstanceOf(NotFoundError);
        });

      // Assert
      expect(mockManager.getRepository).toHaveBeenCalled();
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(findOne).toHaveBeenCalled();
      expect(findOne).toHaveBeenCalledWith(payee.id);
      expect(save).not.toHaveBeenCalled();
    });

    it('should throw when try to edit account payee', async () => {
      // Arrange
      const payee = new Payee({ id: '1', name: 'Account Ent 1 Updated' });
      const mockEntity = new PayeeEntity({ id: '1', name: 'Account Ent 1', isAccount: true });

      const findOne = createSpy('findOne').and.returnValue(Promise.resolve(mockEntity));
      const save = createSpy('save');
      mockManager.getRepository.and.returnValue({ save, findOne });

      // Act
      await payeeRepository.updatePayeeAsync(payee)
        .catch((error: CustomError) => {
          expect(error).toBeDefined();
          expect(error).toBeInstanceOf(CustomError);
          expect(error.message).toBe('Cannot update payee that corresponds to account');
        });

      // Assert
      expect(mockManager.getRepository).toHaveBeenCalled();
      expect(mockManager.getRepository).toHaveBeenCalledWith(PayeeEntity);
      expect(findOne).toHaveBeenCalled();
      expect(findOne).toHaveBeenCalledWith(payee.id);
      expect(save).not.toHaveBeenCalled();
    });
  });
});
