import { Connection, EntityManager } from 'typeorm';
import { PayeeEntity } from 'database/entities';
import { Payee } from 'shared/models';
import { BadRequestError, NotFoundError } from 'src/errors';


export class PayeeRepository {

  constructor(protected dbConnection: Connection) {
  }

  public async getAllPayeesAsync(): Promise<Payee[]> {
    return this.dbConnection.manager.getRepository(PayeeEntity)
      .find()
      .then((payees) => {
        return payees.map((payeeEntity) => new Payee(payeeEntity));
      });
  }

  public async getPayeeByIdAsync(payeeId: string, entityManager: EntityManager = this.dbConnection.manager): Promise<Payee> {
    return entityManager.getRepository(PayeeEntity)
      .findOne(payeeId)
      .then((payee) => {
        return new Payee(payee);
      });
  }

  public async createPayeeAsync(payee: Payee, entityManager: EntityManager = this.dbConnection.manager,
                                options?: { forceAccount: boolean }): Promise<Payee> {
    if (!payee || !payee.name) {
      throw new BadRequestError('Payee name is required');
    }

    if (payee.isAccount && !(options && options.forceAccount)) {
      // Prevent creating isAccount payees
      throw new BadRequestError('Cannot create payee with isAccount set to true');
    }

    return entityManager.getRepository(PayeeEntity)
      .save(new PayeeEntity(payee))
      .then((payeeEntity) => new Payee(payeeEntity));
  }

  public async updatePayeeAsync(payee: Payee, entityManager: EntityManager = this.dbConnection.manager,
                                options?: { forceAccount?: boolean }): Promise<Payee> {
    const foundPayeeEntity = await this.dbConnection.manager.getRepository(PayeeEntity).findOne(payee.id);

    if (!foundPayeeEntity) {
      throw new NotFoundError(Payee.name);
    }

    if (foundPayeeEntity.isAccount && !(options && options.forceAccount)) {
      throw new BadRequestError('Cannot update payee that corresponds to account');
    }

    return entityManager.getRepository(PayeeEntity)
      .save(new PayeeEntity(payee))
      .then((payeeEntity) => new Payee(payeeEntity));
  }

  public async getOrCreatePayeeAsync(payee: Payee, entityManager: EntityManager = this.dbConnection.manager): Promise<Payee> {
    if (payee && payee.id) {
      return this.getPayeeByIdAsync(payee.id);
    } else {
      return this.createPayeeAsync(payee, entityManager);
    }
  }

}
