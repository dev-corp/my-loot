import { MockEntityManager } from 'src/mocks';

import { CategoryRepository } from './category.repository';


describe('CategoryRepository', () => {
  let mockManager: MockEntityManager;
  let categoryRepository: CategoryRepository;

  beforeEach(() => {
    mockManager = new MockEntityManager();
    categoryRepository = new CategoryRepository({ manager: mockManager } as any);
  });

  it('should be created', () => {
    expect(categoryRepository).toBeDefined();
  });
});
