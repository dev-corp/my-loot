import { MockAccountRepository, MockDbQueryUtility, MockEntityManager, MockPayeeRepository } from 'src/mocks';

import { TransactionRepository } from './transaction.repository';


describe('TransactionRepository', () => {
  let mockManager: MockEntityManager;
  let mockDbQueryUtility: MockDbQueryUtility;
  let mockAccountRepository: MockAccountRepository;
  let mockPayeeRepository: MockPayeeRepository;
  let transactionRepository: TransactionRepository;

  beforeEach(() => {
    mockManager = new MockEntityManager();
    mockDbQueryUtility = new MockDbQueryUtility();
    mockAccountRepository = new MockAccountRepository();
    mockPayeeRepository = new MockPayeeRepository();
    transactionRepository = new TransactionRepository({ manager: mockManager } as any,
      mockDbQueryUtility, mockAccountRepository, mockPayeeRepository);
  });

  it('should be created', () => {
    expect(transactionRepository).toBeDefined();
  });
});
