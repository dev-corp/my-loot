import { Brackets, Connection, EntityManager, QueryFailedError } from 'typeorm';
import { IFilter, IQuery } from 'shared/search';
import { Transaction } from 'shared/models';
import { DbQueryUtility } from 'src/utils';
import { TransactionEntity } from 'database/entities';
import { BadRequestError, NotFoundError } from 'src/errors';
import { PayeeRepository } from './payee.repository';
import { AccountRepository } from 'src/repositories/account.repository';


const TRANSACTIONS_ALIAS = 't';

export class TransactionRepository {

  constructor(protected dbConnection: Connection,
              private dbQueryUtility: DbQueryUtility,
              private accountRepository: AccountRepository,
              private payeeRepository: PayeeRepository) {
  }

  public async queryTransactionsAsync(query: IQuery): Promise<{ transactions: Transaction[], total: number }> {

    console.log(query);
    const entityManager = this.dbConnection.manager;

    const dbQuery = entityManager.getRepository(TransactionEntity)
      .createQueryBuilder(TRANSACTIONS_ALIAS)
      .leftJoinAndSelect(`${TRANSACTIONS_ALIAS}.account`, 'account')
      .leftJoinAndSelect(`${TRANSACTIONS_ALIAS}.payee`, 'payee')
      .leftJoinAndSelect(`${TRANSACTIONS_ALIAS}.category`, 'category')
      .leftJoinAndSelect('category.parentCategory', 'parentCategory')
      .leftJoinAndSelect(`${TRANSACTIONS_ALIAS}.transferTransaction`, 'transferTransaction')
    ;

    // Add Filter

    // Find list of u unique aliases so query builder will be able to
    // group same aliases together within brackets and join with OR operator
    const filterAliases = query.filterBy
      .map((filter) => this.dbQueryUtility.getTransactionQueryKey(filter.key, 't').split('.')[0]);
    const uniqueAliases = [...new Set(filterAliases)];

    let parameterIndex = 0;
    uniqueAliases.forEach((uniqueAlias) => {
      const filtersWithSameKey = query.filterBy.filter((filter) => {
        const split = filter.key.split('.');
        split.pop(); // property on alias
        if (split.length === 0) {
          split.push(TRANSACTIONS_ALIAS);
        }
        const alias = split[split.length - 1];
        return alias === uniqueAlias;
      });

      // Group filter on the same alias together with OR within AND
      dbQuery.andWhere(new Brackets((qb) => {
        filtersWithSameKey.forEach((filter) => {
          const paramName = `prop${parameterIndex}`;
          const key = this.dbQueryUtility.getTransactionQueryKey(filter.key, TRANSACTIONS_ALIAS);
          const value = this.dbQueryUtility.parseValueByOperator(filter.operator, filter.value);
          const operator = this.dbQueryUtility.convertQueryOperatorToDbOperator(filter.operator, value);

          if (this.areFiltersRange(filtersWithSameKey)) {
            qb.andWhere(`${key} ${operator} :${paramName}`, { [paramName]: value });
          } else {
            qb.orWhere(`${key} ${operator} :${paramName}`, { [paramName]: value });
          }

          parameterIndex++;
        });
      }));
    });

    try {
      const total = await dbQuery.getCount();

      // Add Sort
      const keysThatAreCaseInsensitive = ['amount', 'date'];
      query.sortBy.forEach((sort) => {
        const key = this.dbQueryUtility.getTransactionQueryKey(sort.key, TRANSACTIONS_ALIAS);
        if (keysThatAreCaseInsensitive.includes(sort.key)) {
          dbQuery.addOrderBy(key, sort.order);
        } else {
          dbQuery.addOrderBy(`LOWER(${key})`, sort.order);
        }
      });

      // Add pagination
      dbQuery.limit(query.limit);
      dbQuery.offset(query.page * query.limit);
      const transactions = await dbQuery.getMany()
        .then((entities) => entities.map((entity) => new Transaction(entity)));

      return { total, transactions };
    } catch (e) {
      if (e instanceof QueryFailedError && e.message.includes('no such column')) {
        throw new BadRequestError('Bad query');
      }
      throw e;
    }
  }

  public async createTransactionAsync(transaction: Transaction, entityManager: EntityManager = this.dbConnection.manager): Promise<Transaction> {
    const transactionEntity = new TransactionEntity(transaction);

    if (transaction.payee) {
      transactionEntity.payee = await this.payeeRepository.getOrCreatePayeeAsync(transactionEntity.payee, entityManager)
        .catch(() => null /* catch wrong payee creation and set it to null */);
    }

    // TODO: Check if category is not parentCategory / do not allow to add transactions to parent category

    const isTransfer = transactionEntity.payee && transactionEntity.payee.isAccount;

    if (isTransfer) {
      return entityManager.save(transactionEntity)
        .then(async (transEnt) => {
          let counterTransferTrans = await this.prepareCounterTransferTransactionAsync(transEnt, entityManager);
          counterTransferTrans = await entityManager.save(counterTransferTrans);
          transEnt.transferTransaction = counterTransferTrans;
          return entityManager.save(transEnt);
        })
        .then((transEnt) => {
          // Prevent RangeError: Maximum call stack size exceeded
          transEnt.transferTransaction.transferTransaction = null;
          return new Transaction(transEnt);
        });
    } else {
      return entityManager
        .save(transactionEntity)
        .then((transEnt) => new Transaction(transEnt));
    }
  }

  public async updateTransactionAsync(transactionToUpdate: Transaction, entityManager?: EntityManager): Promise<Transaction> {
    if (entityManager) {
      return this._updateTransactionAsync(transactionToUpdate, entityManager);
    } else {
      return this.dbConnection.transaction(async (dbTransactionEntityManager) =>
        this._updateTransactionAsync(transactionToUpdate, dbTransactionEntityManager));
    }
  }

  public async deleteTransactionAsync(transactionId: string, entityManager?: EntityManager): Promise<boolean> {
    if (entityManager) {
      return this._deleteTransactionAsync(transactionId, entityManager);
    } else {
      return this.dbConnection.transaction(async (dbTransactionEntityManager) =>
        this._deleteTransactionAsync(transactionId, dbTransactionEntityManager));
    }
  }

  private areFiltersRange(filters: IFilter[]): boolean {
    return filters.filter((filter) => !!filter.operator.match(/[<>]/)).length > 1; // has at least two range operators
  }


  private async _deleteTransactionAsync(transactionId: string, entityManager: EntityManager = this.dbConnection.manager): Promise<boolean> {
    const foundTransaction = await entityManager.findOne<TransactionEntity>(TransactionEntity, transactionId, {
      relations: ['payee', 'account', 'category', 'category.parentCategory', 'transferTransaction'],
    });

    if (!foundTransaction) {
      throw new NotFoundError(Transaction.constructor.name);
    }

    if (foundTransaction.transferTransaction) {
      // Delete transfer
      await entityManager.getRepository(TransactionEntity)
        .delete([foundTransaction.id, foundTransaction.transferTransaction.id])
        .then((result) => result.affected === 1);
    } else {
      return entityManager.getRepository(TransactionEntity)
        .delete(foundTransaction)
        .then((result) => result.affected === 1);
    }
  }

  private async _updateTransactionAsync(transactionToUpdate: Transaction,
                                        entityManager: EntityManager = this.dbConnection.manager): Promise<Transaction> {
    const foundTransaction = await entityManager.findOne<TransactionEntity>(TransactionEntity, transactionToUpdate.id, {
      relations: ['payee', 'account', 'category', 'category.parentCategory', 'transferTransaction'],
    });

    if (!foundTransaction) {
      throw new NotFoundError(Transaction.constructor.name);
    }

    const updatedTransactionEntity = new TransactionEntity({ ...foundTransaction, ...transactionToUpdate });

    if (updatedTransactionEntity.payee) {
      updatedTransactionEntity.payee = await this.payeeRepository.getOrCreatePayeeAsync(updatedTransactionEntity.payee, entityManager)
        .catch(() => null /* catch wrong payee creation and set it to null */);
    }

    // TODO: Check if category is not parentCategory / do not allow to add transactions to parent category

    const wasTransfer = foundTransaction.payee && foundTransaction.transferTransaction;
    const isTransfer = updatedTransactionEntity.payee ? updatedTransactionEntity.payee.isAccount : false;

    if (wasTransfer && isTransfer) {
      // Update transfer transaction
      const counterTransactionEntity = await this.prepareCounterTransferTransactionAsync(updatedTransactionEntity, entityManager);
      updatedTransactionEntity.transferTransaction = counterTransactionEntity;
      return entityManager.save([counterTransactionEntity, updatedTransactionEntity])
        .then(([counterEntity, transactionEntity]) => {
          // Prevent RangeError: Maximum call stack size exceeded
          transactionEntity.transferTransaction.transferTransaction = null;
          return new Transaction(transactionEntity);
        });
    } else if (!wasTransfer && !isTransfer) {
      // Update normal transaction
      updatedTransactionEntity.transferTransaction = null;
      return entityManager.save(updatedTransactionEntity)
        .then((transactionEntity) => new Transaction(transactionEntity));
    } else if (!wasTransfer && isTransfer) {
      // Was normal but changed to transfer transaction
      return entityManager.save(updatedTransactionEntity)
        .then(async (transEnt) => {
          let counterTransferTrans = await this.prepareCounterTransferTransactionAsync(transEnt, entityManager);
          counterTransferTrans = await entityManager.save(counterTransferTrans);
          transEnt.transferTransaction = counterTransferTrans;
          return entityManager.save(transEnt);
        })
        .then((transEnt) => {
          // Prevent RangeError: Maximum call stack size exceeded
          transEnt.transferTransaction.transferTransaction = null;
          return new Transaction(transEnt);
        });
    } else {
      // Was transfer but changed to normal transaction
      updatedTransactionEntity.transferTransaction = null;
      return entityManager.save(updatedTransactionEntity)
        .then((transEnt) => entityManager.delete(TransactionEntity, { id: foundTransaction.transferTransaction.id }).then(() => transEnt))
        .then((transactionEntity) => new Transaction(transactionEntity));
    }
  }

  private async prepareCounterTransferTransactionAsync(transactionEntity: TransactionEntity,
                                                       entityManager: EntityManager = this.dbConnection.manager): Promise<TransactionEntity> {
    const counterTransferTransaction = new TransactionEntity({ ...transactionEntity, ...{ id: undefined } });

    if (!transactionEntity.id) {
      throw new Error('No transaction entity id. ' +
        'Run prepareCounterTransferTransactionAsync with transaction entity that is already saved in database');
    }

    if (transactionEntity.transferTransaction && transactionEntity.transferTransaction.id) {
      counterTransferTransaction.id = transactionEntity.transferTransaction.id;
    }

    counterTransferTransaction.transferTransaction = transactionEntity;
    counterTransferTransaction.account = await this.accountRepository.getAccountByIdAsync(transactionEntity.payee.id, entityManager);
    counterTransferTransaction.payee = await this.payeeRepository.getPayeeByIdAsync(transactionEntity.account.id, entityManager);
    counterTransferTransaction.amount = -1 * counterTransferTransaction.amount;
    return counterTransferTransaction;
  }
}
