import { Connection, EntityManager } from 'typeorm';
import { AccountEntity, CategoryEntity, PayeeEntity, TransactionEntity } from 'database/entities';
import { Account, Payee, Transaction } from 'shared/models';
import { NotFoundError } from 'src/errors';
import { TransactionRepository } from './transaction.repository';
import { PayeeRepository } from './payee.repository';
import { SystemCategoryId, SystemPayeeId } from 'shared/enums';


export class AccountRepository {

  constructor(protected dbConnection: Connection,
              private payeeRepository: PayeeRepository,
              private transactionRepository: TransactionRepository) {
  }

  public getAllAccountsAsync(entityManager: EntityManager = this.dbConnection.manager): Promise<Account[]> {
    return entityManager.getRepository(AccountEntity)
      .find()
      .then((accounts) => {
        return accounts.map((accountEntity) => new Account(accountEntity));
      });
  }

  public async getAccountByIdAsync(id: string, entityManager: EntityManager = this.dbConnection.manager): Promise<Account> {
    return entityManager.getRepository(AccountEntity)
      .findOne(id)
      .then((accountEntity) => {
        return new Account(accountEntity);
      });
  }

  public getAllAccountsWithBalanceAsync(): Promise<Account[]> {
    const queryPromise = this.dbConnection
      .createQueryBuilder(AccountEntity, 'a')
      .leftJoin(TransactionEntity, 't', 't.accountId = a.id')
      .addSelect('ROUND(SUM(t.amount), 2)', 'currentBalance')
      .groupBy('a.id')
      .addGroupBy('a.name')
      .getRawAndEntities();

    return queryPromise.then((result) => {
      return result.entities
        .map((accountEntity, idx) =>
          ({ ...accountEntity, ...{ currentBalance: result.raw[idx].currentBalance } }))
        .map((accountEntity) => new Account(accountEntity));
    });
  }

  public async createAccountAsync(account: Account, startingBalance: number): Promise<Account> {
    return this.dbConnection
      .transaction<AccountEntity>(async (dbtem) => {
        const accountModel = await dbtem.save<AccountEntity>(new AccountEntity(account))
          .then((accountEntity) => new Account(accountEntity));

        // Create payee corresponding to account
        const payee = new Payee({
          id: accountModel.id, // NOTE: Payee id must match account id
          name: accountModel.name,
          isAccount: true,
        });

        await this.payeeRepository.createPayeeAsync(payee, dbtem, { forceAccount: true });

        // Create starting transaction
        if (startingBalance) {
          const startingTransaction = new Transaction({
            date: new Date(),
            account: accountModel,
            amount: startingBalance,
            payee: new PayeeEntity({ id: SystemPayeeId.SystemStartingBalance }),
            category: new CategoryEntity({ id: SystemCategoryId.SystemIncome }),
          });
          const startingTransactionEntity = await this.transactionRepository.createTransactionAsync(startingTransaction, dbtem);
          accountModel.currentBalance = startingTransactionEntity.amount;
        }

        return accountModel;
      })
      .then((accountEntity) => {
        return new Account(accountEntity);
      });
  }

  public async updateAccountAsync(accountToUpdate: Account, balanceAdjust: number): Promise<Account> {
    return this.dbConnection
      .transaction<AccountEntity>(async (dbtem) => {
        const foundAccountEntity = await dbtem.findOne(AccountEntity, accountToUpdate.id);

        if (!foundAccountEntity) {
          throw new NotFoundError(Account.constructor.name);
        }

        const updatedAccount = await dbtem
          .save<AccountEntity>(new AccountEntity({ ...foundAccountEntity, ...accountToUpdate }))
          .then((accountEntity) => new Account(accountEntity));

        let accountPayee = await this.payeeRepository.getPayeeByIdAsync(updatedAccount.id);
        if (accountPayee.name !== updatedAccount.name) {
          accountPayee.name = updatedAccount.name;
          accountPayee = await this.payeeRepository.updatePayeeAsync(accountPayee, dbtem, { forceAccount: true });
        }

        if (!isNaN(balanceAdjust) && balanceAdjust !== null) {
          // get current balance
          const currentBalance = await this.getAccountCurrentBalance(updatedAccount);

          // calculate difference
          const difference = balanceAdjust - currentBalance;

          // add new transaction
          const adjustmentTransaction = new Transaction({
            date: new Date(),
            account: updatedAccount,
            amount: difference,
            payee: new PayeeEntity({ id: SystemPayeeId.SystemAdjustBalance }),
            category: new CategoryEntity({ id: SystemCategoryId.SystemIncome }),
          });

          const adjustmentTransactionEntity = await this.transactionRepository.createTransactionAsync(adjustmentTransaction, dbtem);

          updatedAccount.currentBalance = adjustmentTransactionEntity.amount;
        }

        return updatedAccount;
      })
      .then((accountEntity) => {
        return new Account(accountEntity);
      });
  }

  private async getAccountCurrentBalance(account: Account, entityManger: EntityManager = this.dbConnection.manager): Promise<number> {
    const currentBalanceQuery = entityManger
      .createQueryBuilder(AccountEntity, 'a')
      .leftJoin(TransactionEntity, 't', 't.accountId = a.id')
      .addSelect('ROUND(SUM(t.amount), 2)', 'currentBalance')
      .where('a.id = :id', { id: account.id })
      .groupBy('a.id')
      .addGroupBy('a.name')
      .getRawAndEntities();

    return await currentBalanceQuery.then((result) => {
      console.log(result.raw);
      return result.raw[0].currentBalance as number;
    });
  }
}
