import { Connection, EntityManager } from 'typeorm';
import { CategoryEntity } from 'database/entities';
import { Category } from 'shared/models';
import { CustomError } from 'ts-custom-error';
import { BadRequestError, NotFoundError } from 'src/errors';


export class CategoryRepository {

  constructor(protected dbConnection: Connection) {
  }

  /**
   * At all categories as flat list
   */
  public getAllCategoriesAsync(): Promise<Category[]> {
    return this.dbConnection.getRepository(CategoryEntity)
      .find({ relations: ['childrenCategories', 'parentCategory'] })
      .then((categories) => categories.filter((cat) => cat.parentCategory === null))
      .then((categories) => {
        return categories.map((categoryEntity) => new Category(categoryEntity));
      });
  }

  public async createCategoryAsync(category: Category, entityManager?: EntityManager): Promise<Category> {
    if (entityManager) {
      return this._createCategoryAsync(category, entityManager);
    } else {
      return this.dbConnection.transaction(async (transactionalEntityManager) =>
        this._createCategoryAsync(category, transactionalEntityManager));
    }
  }

  public async updateCategoryAsync(category: Category): Promise<Category> {
    const foundCategory = await this.dbConnection.getRepository(CategoryEntity).findOne({ id: category.id });

    if (!foundCategory) {
      throw new NotFoundError(Category.constructor.name);
    }

    if (foundCategory.isSystem) {
      throw new BadRequestError('Cannot update system category');
    }

    const categoryEntityToUpdate = new CategoryEntity(category);

    return this.dbConnection.getRepository(CategoryEntity).save(categoryEntityToUpdate)
      .then((categoryEntity) => new Category(categoryEntity));
  }

  private async _createCategoryAsync(category: Category, entityManager: EntityManager): Promise<Category> {
    // New parentCategory to create
    const categoryEntity = new CategoryEntity(category);

    if (categoryEntity.parentCategory && !categoryEntity.parentCategory.id) {
      // Create parentCategory category
      return this.createCategoryAsync(new Category(categoryEntity.parentCategory), entityManager)
        // Then create category with newly created parentCategory
        .then((parentCategory) => {
          categoryEntity.parentCategory = new CategoryEntity(parentCategory);
          return this.createCategoryAsync(new Category(categoryEntity), entityManager);
        });
    }

    if (categoryEntity.isSystem) {
      // Prevent creating isAccount payees
      throw new BadRequestError('Cannot create category with isSystem set to true');
    }

    // Prevent adding more levels then 2 (parentCategory and child)
    if (categoryEntity.parentCategory) {
      const parentCategory = await entityManager.getRepository(CategoryEntity)
        .findOne(categoryEntity.parentCategory.id, { relations: ['parentCategory'] });
      if (parentCategory.parentCategory) {
        throw new CustomError('Cannot create child category of another child category. Only 1 sub level allowed');
      }
      categoryEntity.parentCategory = parentCategory;
    }

    return (entityManager || this.dbConnection).getRepository(CategoryEntity)
      .save(categoryEntity)
      .then((catEnt) => new Category(catEnt));
  }
}
