import { EntityManager } from 'typeorm';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;


export class MockEntityManager extends EntityManager {

  public getRepository: Spy = createSpy('getRepository');

  constructor() {
    super(null, null);
  }
}

