import { DbQueryUtility } from 'src/utils';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;


export class MockDbQueryUtility extends DbQueryUtility {

  public mapQueryFieldKeyToDbQueryAlias: Spy = createSpy();
  public convertQueryOperatorToDbOperator: Spy = createSpy();
  public parseValueByOperator: Spy = createSpy();
  public parseQueryParamsToQuery: Spy = createSpy();

  constructor() {
    super();
  }

}
