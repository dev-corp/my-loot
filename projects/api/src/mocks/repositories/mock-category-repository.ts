import { CategoryRepository } from 'src/repositories';
import Spy = jasmine.Spy;
import createSpy = jasmine.createSpy;


export class MockCategoryRepository extends CategoryRepository {

  public getAllCategoriesAsync: Spy = createSpy();
  public createCategoryAsync: Spy = createSpy();
  public updateCategoryAsync: Spy = createSpy();

  constructor() {
    super(null);
  }

}
