import { PayeeRepository } from 'src/repositories';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;


export class MockPayeeRepository extends PayeeRepository {

  public getAllPayeesAsync: Spy = createSpy();
  public getPayeeByIdAsync: Spy = createSpy();
  public createPayeeAsync: Spy = createSpy();
  public updatePayeeAsync: Spy = createSpy();

  constructor() {
    super(null);
  }
}
