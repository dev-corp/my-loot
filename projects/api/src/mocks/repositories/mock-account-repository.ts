import { AccountRepository } from 'src/repositories';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;


export class MockAccountRepository extends AccountRepository {

  public getAllAccountsAsync: Spy = createSpy();
  public getAllAccountsWithBalanceAsync: Spy = createSpy();
  public createAccountAsync: Spy = createSpy();
  public updateAccountAsync: Spy = createSpy();

  constructor() {
    super(null, null, null);
  }

}
