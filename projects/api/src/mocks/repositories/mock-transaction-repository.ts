import { TransactionRepository } from 'src/repositories';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;


export class MockTransactionRepository extends TransactionRepository {

  public queryTransactionsAsync: Spy = createSpy();
  public createTransactionAsync: Spy = createSpy();
  public updateTransactionAsync: Spy = createSpy();
  public deleteTransactionAsync: Spy = createSpy();

  constructor() {
    super(null, null, null, null);
  }
}
