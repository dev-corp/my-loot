import { CustomError } from 'ts-custom-error';


export class QueryParsingError extends CustomError {
  constructor(message: string) {
    super(`Error while parsing query. ${message}`);
  }
}
