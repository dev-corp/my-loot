import { CustomError } from 'ts-custom-error';


export class BadRequestError extends CustomError {

  constructor(message: string) {
    super(message);
  }
}
