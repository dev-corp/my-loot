import { CustomError } from 'ts-custom-error';


export class NotFoundError extends CustomError {

  constructor(entity: string) {
    super(`${entity} not found`);
  }
}
