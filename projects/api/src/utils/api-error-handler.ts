import { NextFunction, Request, Response } from 'express';
import { constants } from 'http2';
import { CustomError } from 'ts-custom-error';
import { QueryFailedError } from 'typeorm';
import { BadRequestError, NotFoundError, QueryParsingError } from 'src/errors';


export function apiErrorHandlerFactory(environment: string): (err: Error, req: Request, res: Response, next: NextFunction) => void {
  return (err: Error, req: Request, res: Response, next: NextFunction) => {
    // API Errors
    if (err instanceof CustomError) {
      if (err instanceof BadRequestError || err instanceof QueryParsingError) {
        res.status(constants.HTTP_STATUS_BAD_REQUEST);
      }
      if (err instanceof NotFoundError) {
        res.status(constants.HTTP_STATUS_NOT_FOUND);
      }

      res.status(constants.HTTP_STATUS_BAD_REQUEST);
    }

    // TypeORM / DB Errors
    if (err instanceof QueryFailedError) {
      // TODO: Test this behaviour on MySQL
      if (err.message.includes('UNIQUE constraint failed')) {
        res.status(constants.HTTP_STATUS_CONFLICT);
      }
      if (err.message.includes('FOREIGN KEY constraint failed')) {
        res.status(constants.HTTP_STATUS_BAD_REQUEST);
      }
      if (err.message.includes('no such column')) {
        res.status(constants.HTTP_STATUS_BAD_REQUEST);
      }
    }

    // Completes
    if (environment === 'production') {
      // TODO: Check if default error handler hides stacktrace on production
      res.send(err.message);
      return;
    }
    next(err);
  };
}
