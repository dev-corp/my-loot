import { IQuery, FilterOperator, QueryUtility } from 'shared/search';


export class DbQueryUtility {

  /**
   * Parses filter or sort key from key to db query relation key
   * IMPORTANT:The basic db query select must be build so the
   * relation aliases are always the name of it parentCategory - this is
   * super important with nested relations
   * E.g. transaction.category.parentCategory -> transaction.[category] & category.[parentCategory]
   * names in [brackets] are the aliases
   */
  public getTransactionQueryKey(filterOrSortKey: string, baseEntityAlias: string): string {
    const split = filterOrSortKey.split('.');
    if (split.length > 1) {
      const key = split[split.length - 2]; // second last
      const property = split[split.length - 1]; // first last
      return `${key}.${property}`;
    }
    return `${baseEntityAlias}.${split[0]}`;
  }

  public convertQueryOperatorToDbOperator(operator: FilterOperator | string, value: string | number | null): string {
    return QueryUtility.convertQueryOperatorToDbOperator(operator, value);
  }

  public parseValueByOperator(operator: FilterOperator | string, value: string | number): string | number {
    return QueryUtility.parseValueByOperator(operator, value);
  }

  public parseQueryParamsToQuery(queryParams: any, defaultLimit: number = 100): IQuery {
    return QueryUtility.queryParamsToQuery(queryParams, defaultLimit);
  }
}
