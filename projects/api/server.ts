import { ConnectionOptions } from 'typeorm';
import { MyLootApi } from './src';
import * as commandLineArgs from 'command-line-args';
import * as commandLineUsage from 'command-line-usage';
import * as fs from 'fs';
import * as path from 'path';


interface IOptions {
  env?: string;
  name?: string;
  ormconfig?: string;
  runMigrations?: boolean;
  help?: boolean;
}

const optionsDefinition = [
  { name: 'env', alias: 'e', type: String, typeLabel: 'string', description: 'environment [production, development]' },
  {
    name: 'ormconfig',
    alias: 'c',
    type: String/*, defaultOption: 'ormconfig.json'*/,
    typeLabel: 'file',
    description: 'ormconfig.json file path',
  },
  {
    name: 'name',
    alias: 'n',
    type: String/*, defaultOption: 'sqlite'*/,
    typeLabel: 'string',
    description: 'orm config name (if ormconfig is array) [sqlite, mysql]',
  },
  {
    name: 'runMigrations',
    alias: 'r',
    type: Boolean/*, defaultOption: 'sqlite'*/,
    typeLabel: 'boolean',
    description: 'defines if server should run db migrations',
  },
  { name: 'help', alias: 'h', type: Boolean, defaultOption: false, typeLabel: 'boolean', description: 'show help' },
];

const options: IOptions = commandLineArgs(optionsDefinition);

if (options.help || !Object.keys(options).length) {
  // Print help
  const usage = commandLineUsage([
    {
      header: 'My Loot API Server',
      optionList: optionsDefinition,
    },
  ]);
  console.log(usage);

} else {
  // Run server
  if (!options.ormconfig) {
    throw new Error('--ormconfig is required');
  }

  const ormconfigPath = path.join(process.cwd(), options.ormconfig);
  const ormconfig = JSON.parse(fs.readFileSync(ormconfigPath).toString());
  const connectionOptions = Array.isArray(ormconfig)
    ? ormconfig.find((c) => c.name === options.name)
    : ormconfig;

  // Delete name so connection become default
  const connectionName = connectionOptions.name;
  delete connectionOptions.name;

  if (!process.env.NODE_ENV) {
    if (options.env) {
      process.env.NODE_ENV = options.env;
    } else {
      process.env.NODE_ENV = 'development';
    }
  }

  if (process.env.NODE_ENV === 'production') {
    connectionOptions.logging = false;
  }

  console.log('Environment:', process.env.NODE_ENV);
  console.log('Connection name:', connectionName);
  console.log('Connection options:', connectionOptions);
  console.log('Running migrations:', !!options.runMigrations);
  console.log();

  connectionOptions.migrationsRun = !!options.runMigrations;

  const server = new MyLootApi(connectionOptions as ConnectionOptions);
}
